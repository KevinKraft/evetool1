#Tool for calculating margins for making t3 strategic cruisers at today's date
#08/05/20

#todo: The job cost in game is 10 times smaller, but it doesn't make much of a difference. The item base price was
# only 2 mil in game, but its more like 10 mil.

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.fuel_items import FUEL_GROUPS

from group_id_report import group_id_report

#-------------------------------------

GROUP_ID = 963

PRICE_MODE = 'sell'

RUNS = 1

SAVE_FILE = 'data/strategic_cruisers.csv'

SAVE_RESULTS = True

FUEL_ITEM_GROUPS = FUEL_GROUPS.STRATEGIC_CRUISER

#-------------------------------------

def main():

    group_id_report(GROUP_ID, PRICE_MODE, RUNS, SAVE_FILE, SAVE_RESULTS, FUEL_ITEM_GROUPS)


if __name__ == '__main__':
    main()
