#Tool for the margins for all items in the game at todays prices

#todo: The job cost in game is 10 times smaller, but it doesn't make much of a difference. The item base price was
# only 2 mil in game, but its more like 10 mil.

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)
import datetime

from src.constants.id import STRUCTURE_ID
from src.constants.tax_rate import TAX_RATE

from src.write.save_as_df import SaveRowAsCsv
from src.metadata.read import get_all_blueprints, get_blueprint_product, get_group
from src.object.recipe.recipe import get_recipe_from_blueprint

#-------------------------------------

STRUCTURE = STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER
#STRUCTURE = STRUCTURE_ID.JITA_TRADE_HUB

SALEX_TAX = TAX_RATE[STRUCTURE]['sales']
BROKER_TAX = TAX_RATE[STRUCTURE]['broker']
FULL_SALES_TAX = SALEX_TAX + BROKER_TAX

SYSTEM_COST_INDEX = 0.03 #3%
FACILITY_TAX_RATE = 0.1 #10%

BP_IDS = get_all_blueprints()

PRICE_MODE = 'buy'

RUNS = 1

SAVE_FILE = 'data/all_recipes.csv'

SAVE_RESULTS = False

#-------------------------------------

def main():

    profits = []
    profit_margins = []
    profit_percentages = []
    markups = []
    cost_prices = []
    bp_names = []
    bp_ids = []
    bp_prod_ids = []
    bp_prod_group_ids = []

    n_bps = len(BP_IDS)
    for i, bp_id in enumerate(BP_IDS):
        if i % 100 == 0:
            print(f'Processing blueprint {bp_id} (i={i} of {n_bps})')

        reaction = get_recipe_from_blueprint(RUNS, bp_id, price_mode=PRICE_MODE)
        if reaction is None:
            continue

        #reaction.job_summary(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)
        profits.append((reaction.name, reaction.profit(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)))
        profit_margins.append((reaction.name, reaction.profit_margin(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                     FACILITY_TAX_RATE)))
        profit_percentages.append((reaction.name, reaction.profit_percentage(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                     FACILITY_TAX_RATE)))
        markups.append((reaction.name, reaction.markup(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                     FACILITY_TAX_RATE)))
        cost_prices.append((reaction.name, reaction.cost_price(FULL_SALES_TAX)))
        bp_names.append(reaction.name)
        bp_ids.append(bp_id)
        bp_prod_id = get_blueprint_product(bp_id)
        bp_prod_ids.append(bp_prod_id)
        bp_prod_group_ids.append(get_group(bp_prod_id))


        #print('*'*80)

    if SAVE_RESULTS:

        csv_saver = SaveRowAsCsv(SAVE_FILE, ['timestamp', 'bp', 'profit', 'profit_margin',
                                             'profit_percentage', 'markup', 'cost_price', 'runs',
                                             'system_cost_index', 'facility_tax_rate',
                                             'full_sales_tax', 'buy_inputs', 'bp_id', 'product_id', 'group_id'])

        def _fv(val):
            return round(val[1], 2)

        def _assert_match(val, name):
            assert val[0] == name

        for p, pm, pp, mu, cp, name, bp_id, product_id, group_id in zip(profits, profit_margins, profit_percentages,
                                                                        markups, cost_prices, bp_names, bp_ids,
                                                                        bp_prod_ids, bp_prod_group_ids):
            _assert_match(p, name)
            _assert_match(pm, name)
            _assert_match(pp, name)
            _assert_match(mu, name)
            _assert_match(cp, name)

            csv_saver.add_data([datetime.datetime.now().isoformat(), name,
                                _fv(p), _fv(pm), _fv(pp), _fv(mu), _fv(cp),
                                RUNS, SYSTEM_COST_INDEX, FACILITY_TAX_RATE, FULL_SALES_TAX,
                                PRICE_MODE, bp_id, product_id, group_id])

    else:

        print('Profit:')
        profits = sorted(profits, key=lambda x: x[1], reverse=True)
        for name, profit in profits:
            print(name, profit)
        print('Profit margins:')
        vals = sorted(profit_margins, key=lambda x: x[1], reverse=True)
        for name, val in vals:
            print(name, str(round(val, 2)))
        print('Profit percentages:')
        vals = sorted(profit_percentages, key=lambda x: x[1], reverse=True)
        for name, val in vals:
            print(name, str(round(val,2)))
        print('Markup:')
        vals = sorted(markups, key=lambda x: x[1], reverse=True)
        for name, val in vals:
            print(name, str(round(val,2)))
        print('Cost prices:')
        vals = sorted(cost_prices, key=lambda x: x[1], reverse=True)
        for name, val in vals:
            print(name, str(round(val,2)))




if __name__ == '__main__':
    main()