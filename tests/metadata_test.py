import sys

sys.path.append('../')

from src.constants.id import ITEM_ID
from src.constants.fuel_items import FUEL_GROUPS

from src.metadata.read import get_name, get_type_from_name, get_group, get_group_name, get_all_types, get_blueprint, \
    get_recipe_from_bp, get_recipe as read_database_recipe, get_volume, get_all_types_from_database
from src.metadata.util import PostgresDatabase
from src.model.metadata import PostgresDatabaseSchema
from src.metadata.schema import EVE_DB_SCHEMA
from src.object.item.eve_item import get_item
from src.object.recipe.recipe import get_recipe

#-------------------------------------

SCHEMA = PostgresDatabaseSchema(EVE_DB_SCHEMA)
DATABASE = PostgresDatabase(SCHEMA)

#-------------------------------------



def main():

    #
    # has_table = DATABASE.has_table('type_id')
    # print('has_table: ', has_table)
    #
    # has_table_temp = DATABASE.has_table_temp('type_id')
    # print('has_table_temp: ', has_table_temp)
    #
    # read_table = DATABASE.read_table('type_id', ['name'], filter_map={'id': ITEM_ID.SCANDIUM_METALLOFULLERENE})
    # print('read_table: ', read_table)
    #
    # read_table_temp = DATABASE.read_table_temp('type_id', ['name'], filter_map={'id': ITEM_ID.SCANDIUM_METALLOFULLERENE})
    # print('read_table_temp: ', read_table_temp)
    #
    # group = get_group(ITEM_ID.SCANDIUM_METALLOFULLERENE)
    # print('group: ', group)
    #
    # read_table_mul = DATABASE.read_table('type_id', ['id'], filter_map={'groupid': group})
    # print('read_table_mul: ', read_table_mul)
    #
    # read_table_mul_temp = DATABASE.read_table_temp('type_id', ['id'], filter_map={'groupid': group})
    # print('read_table_mul_temp: ', read_table_mul_temp)


    name = get_name(ITEM_ID.SCANDIUM_METALLOFULLERENE)
    print('name: ', name)

    type_id = get_type_from_name(name)
    print('type: ', type_id)

    group = get_group(type_id)
    print('group: ', group)

    group_name = get_group_name(group)
    print('group_name: ', group_name)

    all_types = get_all_types(group)
    print('all_types: ', all_types)

    bp = get_blueprint(type_id)
    print('bp: ', bp)

    recipe = get_recipe_from_bp(bp)
    print('recipe: ', recipe.to_primitive())

    recipe = read_database_recipe(type_id)
    print('recipe: ', recipe.to_primitive())

    volume = get_volume(type_id)
    print('volume: ', volume)

    item = get_item(type_id, amount=100, min_amount=100)
    print('item: ', item)

    #Get some of the group IDs for fuel items
    fuel_block_group = get_group(4246)
    print('fuel_block_group: ', fuel_block_group)
    mineral_group = get_group(34)
    print('mineral_group: ', mineral_group)

    recipe = get_recipe(100, type_id, price_mode='buy', average_mode=False, date=None, fuel_item_groups=FUEL_GROUPS.REACTION)
    print('recipe: ', recipe)

    print('recipe._input_items: ', [str(i) for i in recipe._input_items])
    print('recipe._output_items: ', [str(i) for i in recipe._output_items])
    print('recipe._fuel_items: ', [str(i) for i in recipe._fuel_items])

    print('recipe.input_items: ', [str(i) for i in recipe.input_items])
    print('recipe.output_items: ', [str(i) for i in recipe.output_items])

    all_game_types = get_all_types_from_database()
    print('all_game_types: ', all_game_types)

if __name__ == '__main__':
    main()