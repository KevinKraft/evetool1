import json
import requests

from src.constants.id import REGION_ID
from src.constants.id import STRUCTURE_ID

#-------------------------------------

EVEMARKETER = 'https://api.evemarketer.com'

EVE_DEVELOPERS = 'https://api-sisi.testeveonline.com'

EVE_ESI = 'https://esi.evetech.net/latest'

#-------------------------------------

#https://esi.evetech.net/ui/

def main():

    headers = {'Content-Type': 'application/json'}

    print('*'*80)

    response = requests.get(EVEMARKETER+'/ec/marketstat/json?typeid=1230')
    print(response)
    res = json.loads(response.content.decode('utf-8'))
    print(res)

    print('*'*80)

    response = requests.get(EVE_DEVELOPERS+'/market/10000002/orders/buy/?type=1230')
    print(response)
    res = json.loads(response.content.decode('utf-8'))
    print(res)

    print('*'*80)

    #all item prices, probably in the whole universe as an average
    response = requests.get(EVE_ESI+'/markets/prices/')
    print(response)
    res = json.loads(response.content.decode('utf-8'))
    #print(res)

    print('*'*80)

    #high, low, av, volume for forge starting from yesterday
    response = requests.get(EVE_ESI+'/markets/10000002/history/?type_id=1230')
    print(response)
    res = json.loads(response.content.decode('utf-8'))
    #print(res)
    print('*'*10)
    res = list(filter(lambda x: '2019-11-22' in x['date'], list(res)))
    print(res)

    print('*'*80)
    #this looks like everything in the region, all buy and sell.
    response = requests.get(EVE_ESI+'/markets/10000002/orders/')
    print(response)
    res = json.loads(response.content.decode('utf-8'))
    res = sorted(list(res), key=lambda x: x['price'])
    print(res[0])

    print('*'*80)
    #list of type IDs with active orders.
    response = requests.get(EVE_ESI+'/markets/10000002/types/?region_id='+str(REGION_ID.THE_FORGE))
    print(response)
    res = json.loads(response.content.decode('utf-8'))
    print(res)

    print('*'*80)
    #Orders in a structure, requires authorization, so not working.
    response = requests.get(EVE_ESI+'/markets/structures/'+str(STRUCTURE_ID.JITA_TRADE_HUB)+'/', headers=headers)
    print(response)
    res = json.loads(response.content.decode('utf-8'))
    print(res)

if __name__ == '__main__':
    main()