#Script for plotting the recipe pull data over time from the all recipes average csv file
#30/05/20

#-------------------------------------

import os
import logging
logging.getLogger().setLevel(logging.INFO)
import matplotlib.pyplot as plt
from matplotlib._color_data import TABLEAU_COLORS
import pandas
from math import ceil
import pickle
import datetime

from src.metadata.read import get_blueprint_product, get_all_types, get_blueprint, get_name

#-------------------------------------

DATA_FILE = 'data/all_recipes_average.csv'

NAME_COL = 'bp'
MARGIN_COL = 'profit_margin'
PP_COL = 'profit_percentage'
PROFIT_COL = 'profit'
COST_PRICE_COL = 'cost_price'
TIMESTAMP_COL = 'timestamp'
BP_ID_COL = 'bp_id'

BASE_FIG_PATH = 'data/plots/'

BP_IDS = [get_blueprint(t) for t in get_all_types(974)] #hybrid polymers

DATA_TYPES = [MARGIN_COL, PP_COL, PROFIT_COL, COST_PRICE_COL]

PLOT_PROFITABLE = False

FROM_DATE = datetime.date(year=2019, month=4, day=1)
TO_DATE = datetime.date(year=2020, month=5, day=29)

#37 bins is about 10 days per bin over a year long interval.
NBINS = 37
#NBINS = 12
#NBINS = 5

#-------------------------------------

NAME_ATTR = 'name'
STYLE_DICT = {
    MARGIN_COL : {NAME_ATTR: 'Margin (%)'},
    PP_COL: {NAME_ATTR: 'Profit Percentage (%)'},
    PROFIT_COL: {NAME_ATTR: 'Profit (ISK)'},
    COST_PRICE_COL: {NAME_ATTR: 'Cost Price (ISK)'},
}

CB_COLOUR_CYCLE = list(TABLEAU_COLORS.values())

#-------------------------------------

def plot_distributions(df, bp_id):

    df = df.loc[df[BP_ID_COL] == bp_id]

    nheight = int(ceil(len(DATA_TYPES)/2))

    fig, axs_ar = plt.subplots(nheight, 2)
    fig.set_figheight(15)
    fig.set_figwidth(15)

    recipe_name = get_name(get_blueprint_product(bp_id))
    to_date = str(TO_DATE)
    from_date = str(FROM_DATE)

    axs = []
    for xi in range(0, nheight):
        for yi in range(0, 2):
            axs.append(axs_ar[xi, yi])

    handles, labels = None, None
    for plot_index in range(len(axs)):

        ax= axs[plot_index]

        if plot_index == len(axs)-1:
            ax.set_axis_off()
            legend_font_norm = 1
            legend_font_size = 170 / legend_font_norm
            if legend_font_size > 18:
                legend_font_size = 18
            ax.legend(handles, labels, fontsize=legend_font_size)
            break

        data_type = DATA_TYPES[plot_index]


        df = df.loc[(df[TIMESTAMP_COL] >= from_date) & (df[TIMESTAMP_COL] <= to_date)]
        #xs = sorted(df[data_type])
        xs = df[data_type]

        ax.hist(xs, bins=NBINS, label=recipe_name, color=CB_COLOUR_CYCLE[0], alpha=0.5)
        ax.grid(True)

        ax.set_xlabel(STYLE_DICT[data_type][NAME_ATTR], fontsize=16)

        handles, labels = ax.get_legend_handles_labels()


    date_now = str(datetime.date.today()).replace('-','_')

    fig_name = recipe_name + ' from ' + from_date + ' to ' + to_date
    fig.suptitle(fig_name, fontsize=24)

    file_name = BASE_FIG_PATH + recipe_name.replace('-','_') + '_distributions_' + date_now

    pickle.dump(fig, open(file_name + '.pickle', 'wb'))

    plt.savefig(file_name + '.pdf')

def main():

    df = pandas.read_csv(DATA_FILE, delimiter=',', encoding='windows-1252')

    for bp in BP_IDS:
        plot_distributions(df, bp)


if __name__ == '__main__':
    main()