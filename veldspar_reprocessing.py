#Tool for calculating veldspar preprocessing profit
#19/11/20

#THIS NO LONGER USES THE MAIN CODE. IT USES THE OLD HARDCODED ITEM CLASSES THE NEW ITEM CLASSES DONT HAVE THE
#REPROCESSING INFORMATION. THE REPROCESING INFORMATION SHOULD BE ADDED AS RECIPES.

#OBSOLETE

#-------------------------------------

import sys
import logging

from src.constants.id import  ITEM_ID

from src.api.api import get_min_sell_price as msp, get_max_buy_price as mbp
from src.util.isk_amount import ISKAmount

#-------------------------------------

#REPROCESSING_RATE = 0.551
REPROCESSING_RATE = 0.767
BUY = 'buy'
SELL = 'sell'
STRUCTURE_TAX_RATE = 0.003 #0.3%
ORES_TO_CONSIDER = [
    'veldspar', 'dense_veldspar',
    #'stable_veldspar',
    'concentrated_veldspar',
    'compressed_veldspar', 'compressed_dense_veldspar', 'compressed_stable_veldspar', 'compressed_concentrated_veldspar'
]
SALES_TAXES = 0.005 + 0.028 #0.5% and 2.8%

#-------------------------------------

MIASMOS_ORE_HOLD = 42000
OBELISK_HOLD = 440000

#-------------------------------------

ITEM_BUY_SELL = {
    'veldspar'       : {BUY: msp(ITEM_ID.VELDSPAR, 50000), SELL: None},
    'dense_veldspar' : {BUY: msp(ITEM_ID.DENSE_VELDSPAR, 50000), SELL: None},
    #''stable_veldspar': {BUY: msp(ITEM_ID.STABLE_VELDSPAR, 50000), SELL: None},
    'concentrated_veldspar' : {BUY: msp(ITEM_ID.CONCENTRATED_VELDSPAR, 50000), SELL: None},
    'compressed_veldspar' : {BUY: msp(ITEM_ID.COMPRESSED_VELDSPAR, 1000), SELL: None},
    'compressed_dense_veldspar' : {BUY: msp(ITEM_ID.COMPRESSED_DENSE_VELDSPAR, 1000), SELL: None},
    'compressed_stable_veldspar' : {BUY: msp(ITEM_ID.COMPRESSED_STABLE_VELDSPAR, 1000), SELL: None},
    'compressed_concentrated_veldspar' : {BUY: msp(ITEM_ID.COMPRESSED_CONCENTRATED_VELDSPAR, 1000), SELL: None},
    'tritanium'      : {BUY: None, SELL: mbp(ITEM_ID.TRITANIUM)}
}

#-------------------------------------

# class EveItem(object):
#
#     _name = None
#     _unit_volume = None
#
#     def __init__(self, amount = 1, sales_tax = SALES_TAXES):
#         if self._name != None and self._name in ITEM_BUY_SELL.keys():
#             self._unit_buy = self._make_price(ITEM_BUY_SELL[self._name][BUY])
#             self._unit_sell = self._make_price(ITEM_BUY_SELL[self._name][SELL])
#         else:
#             logging.error('No buy and sell prices supplied for item {}'.format(self._name))
#             sys.exit(0)
#         if amount != None:
#             self.amount = amount
#         self._sales_tax = sales_tax
#
#     def __str__(self):
#         return 'name: {}, amount {}'.format(self._name, self.amount)
#
#     @property
#     def amount(self):
#         return self.___amount
#
#     @amount.setter
#     def amount(self, val):
#         self.___amount = val
#
#     @property
#     def buy_price(self):
#         return self.amount * self.unit_buy
#
#     @property
#     def name(self):
#         return self._name
#
#     def sell_price(self):
#         value = self._base_sell_price()
#         return value - (value * self._sales_tax)
#
#     @property
#     def unit_buy(self):
#         return self._unit_buy
#
#     @property
#     def unit_sell(self):
#         return self._unit_sell
#
#     def amount_in_given_volume(self, given_volume):
#         return floor(given_volume / self._unit_volume)
#
#     def volume(self):
#         if self._unit_volume == None or self.amount == None or self.amount < 0:
#             logging.warn('Item has no volume as the amount ({}) or unit volume are not set ({})'
#                          .format(self.amount, self._unit_volume))
#         return self._unit_volume * self.amount
#
#     def sales_tax(self):
#         return self._base_sell_price() * self._sales_tax
#
#     def set_amount_to_fill(self, given_volume):
#         self.amount = self.amount_in_given_volume(given_volume)
#
#     def _base_sell_price(self):
#         return self.amount * self.unit_sell
#
#     def _make_price(self, price):
#         if price == None: return None
#         return ISKAmount(price)
#
#
# class MineralItem(EveItem):
#     pass
#
#
# class Tritanium(MineralItem):
#
#     _name = 'tritanium'
#     _unit_volume = 0.01
#
#
# class OreItem(EveItem):
#
#     _reprocessed_materials = None
#     _reprocess_load = None
#
#     def reprocessed_materials(self, reprocessing_rate):
#         loads = floor(self.amount / self._reprocess_load + 0.0)
#         perfect = self._reprocessed_materials.amount * loads
#         amount = (perfect * reprocessing_rate)
#         return self._reprocessed_materials.__class__(amount)
#
#     def reprocessing_fee(self, tax_rate):
#         # todo: In the game this is either floored or rounded with .5 goes to .0, not sure which.
#         return ISKAmount(self.buy_price * tax_rate)
#
#
# class Veldspar(OreItem):
#     _name = 'veldspar'
#     _unit_volume = 0.1
#     _reprocessed_materials = Tritanium(415)
#     _reprocess_load = 100
#
#
# class StableVeldspar(Veldspar):
#     _name = 'stable_veldspar'
#     _reprocessed_materials = Tritanium(477)
#
#
# class DenseVeldspar(Veldspar):
#     _name = 'dense_veldspar'
#     _reprocessed_materials = Tritanium(457)
#
#
# class ConcentratedVeldspar(Veldspar):
#     _name = 'concentrated_veldspar'
#     _reprocessed_materials = Tritanium(436)
#
#
# class CompressedVeldspar(Veldspar):
#     _name = 'compressed_veldspar'
#     _unit_volume = 0.15
#     _reprocess_load = 1
#
#
# class CompressedStableVeldspar(StableVeldspar):
#     _name = 'compressed_stable_veldspar'
#     _unit_volume = 0.15
#     _reprocess_load = 1
#
#
# class CompressedDenseVeldspar(DenseVeldspar):
#     _name = 'compressed_dense_veldspar'
#     _unit_volume = 0.15
#     _reprocess_load = 1
#
#
# class CompressedConcentratedVeldspar(ConcentratedVeldspar):
#     _name = 'compressed_concentrated_veldspar'
#     _unit_volume = 0.15
#     _reprocess_load = 1
#
#
# ALL_ITEMS = [EveItem,
#              MineralItem,
#              Tritanium,
#              OreItem,
#              Veldspar, StableVeldspar, DenseVeldspar, ConcentratedVeldspar,
#              CompressedVeldspar, CompressedStableVeldspar, CompressedDenseVeldspar, CompressedConcentratedVeldspar
#              ]

#-------------------------------------

class ProfitCalculator(object):

    def __init__(self):
        pass

    @staticmethod
    def base_profit(initial_item, final_item):
        return ISKAmount(final_item.sell_price - initial_item.buy_price)


    @classmethod
    def profit(cls, initial_item, final_item, taxes_and_charges):
        base_profit = cls.base_profit(initial_item, final_item)
        return base_profit - taxes_and_charges


    #@staticmethod
    #def is_it_cheaper_to_buy_or_make(self, initial_item, final_item):
    #    make_cost = initial_item.buy_price
    #    buy_cost = final_item.buy_price
    #    if buy_cost < make_cost:


#-------------------------------------


def amount_in_miasmos(given_item):
    return given_item.amount_in_given_volume(MIASMOS_ORE_HOLD)


def amount_in_obelisk(given_item):
    return given_item.amount_in_given_volume(OBELISK_HOLD)


def miasmos_full_of_item_profitability(item_class, reprocessing_rate, tax_rate):

    pc = ProfitCalculator()
    v = item_class()
    v.set_amount_to_fill(MIASMOS_ORE_HOLD)
    print('buy price: '+str(v.buy_price))

    t = v.reprocessed_materials(reprocessing_rate)
    print('sell reprocessed price: '+str(t.sell_price))

    job_fee = v.reprocessing_fee(tax_rate)
    sales_tax = t.sales_tax()
    tax = job_fee + sales_tax
    print('Tax: '+str(tax))

    profit = pc.profit(v, t, tax)
    print('Profit: '+str(profit.mil))

    return v, t, tax, profit

def given_amount_of_ore_profitability(item_class, reprocessing_rate, tax_rate, amount):

    pc = ProfitCalculator()
    v = item_class(amount)
    print('buy price: '+str(v.buy_price))

    t = v.reprocessed_materials(reprocessing_rate)
    print('sell reprocessed price: '+str(t.sell_price))

    job_fee = v.reprocessing_fee(tax_rate)
    sales_tax = t.sales_tax()
    tax = job_fee + sales_tax
    print('Tax: '+str(tax))

    profit = pc.profit(v, t, tax)
    print('Profit: '+str(profit.mil))

    return v, t, tax, profit

def find_item_class_by_name(name):

    for item_class in ALL_ITEMS:
        if item_class._name == name:
            return item_class
    else:
        logging.error('No matching item class type for name "{}"'.format(name))
        sys.exit(0)

def main():

    profits = {}
    v_amounts = {}
    v_costs = {}
    taxes = {}
    t_amounts = {}

    for item_name in ORES_TO_CONSIDER:

        print('*'*80)
        item_class = find_item_class_by_name(item_name)

        v, t, taxes[item_name], profits[item_name] = miasmos_full_of_item_profitability(item_class,
                                                                             REPROCESSING_RATE,
                                                                             STRUCTURE_TAX_RATE)
        # v, t, taxes[item_name], profits[item_name] = given_amount_of_ore_profitability(item_class,
        #                                                                                REPROCESSING_RATE,
        #                                                                                STRUCTURE_TAX_RATE,
        #                                                                                MIASMOS_ORE_HOLD)

        v_amounts[item_name] = v.amount
        t_amounts[item_name] = t.amount
        v_costs[item_name] = v.buy_price


    profit_tup_list = [ (key, value) for key, value in profits.items() ]
    sorted_profits = sorted(profit_tup_list, key=lambda x: x[1], reverse=True)

    print('*'*80)
    print('Results:')
    print('{}: {}: {}: {}: {}: {}'.format('ore'.ljust(34),
                                  'profit'.ljust(20),
                                  'ore amount'.ljust(20),
                                  'seed cost'.ljust(20),
                                  'tax (job fee, sales, broker)'.ljust(30),
                                  'trit amount'.ljust(20),
                                  ))
    for key, val in sorted_profits:
        v_amount = v_amounts[key]
        t_amount = t_amounts[key]
        v_cost = v_costs[key]
        tax = taxes[key]
        print('{}: {}: {}: {}: {}: {}'.format(key.ljust(34),
                                      val.mil.ljust(20),
                                      '{:,}'.format(v_amount).ljust(20),
                                      str(v_cost).ljust(20),
                                      str(tax).ljust(30),
                                              '{:,}'.format(t_amount).ljust(20),
                                              ))





if __name__ == '__main__':
    main()