#Tool for saving historic margin data for all recipes to a csv file
#16/05/20

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)
import datetime

from src.constants.id import STRUCTURE_ID
from src.constants.tax_rate import TAX_RATE

from src.object.recipe.hybrid_reaction import *
from src.write.save_as_df import CSVIOWriter
from src.metadata.read import get_blueprint_product, get_group, get_group_name, get_all_types, get_blueprint, get_name
from src.object.recipe.recipe import get_recipe_from_blueprint

from src.util.diagnostic import Timer

#-------------------------------------

STRUCTURE = STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER
#STRUCTURE = STRUCTURE_ID.JITA_TRADE_HUB

SALEX_TAX = TAX_RATE[STRUCTURE]['sales']
BROKER_TAX = TAX_RATE[STRUCTURE]['broker']
FULL_SALES_TAX = SALEX_TAX + BROKER_TAX

SYSTEM_COST_INDEX = 0.03 #3%
FACILITY_TAX_RATE = 0.1 #10%

#974 - hybrid polymers
BP_IDS = [get_blueprint(t) for t in get_all_types(974)] #sorted(get_all_blueprints())[4000:]

NEW_FILE = False

RUNS = 1

PRICE_MODE = 'buy'

SAVE_FILE = 'data/hybrid_polymer_average.csv'

SAVE_RESULTS = True

Timer.running = False

#-------------------------------------

START_DATE = datetime.date(year=2020, month=7, day=3)
#INCLUSIVE_END_DATE = datetime.date(year=2019, month=4, day=1)  # this is the oldest date the api has data as of 30/05/20
INCLUSIVE_END_DATE = datetime.date(year=2020, month=5, day=30)
# INCLUSIVE_END_DATE = datetime.date(year=2020, month=3, day=8)
# INCLUSIVE_END_DATE = datetime.date(year=2020, month=1, day=14) #small sample testing

#-------------------------------------

def main():

    csv_saver = CSVIOWriter(SAVE_FILE, headers=['timestamp', 'bp', 'profit', 'profit_margin', 'profit_percentage',
                                                'cost_price', 'runs', 'system_cost_index', 'facility_tax_rate',
                                                'full_sales_tax', 'buy_inputs', 'bp_id', 'product_id', 'group_id',
                                                'group_name'],
                            new_file=NEW_FILE)

    start_date = START_DATE
    inclusive_end_date = INCLUSIVE_END_DATE

    n_bps = len(BP_IDS)
    for bn, bp_id in enumerate(BP_IDS):

        print(f'Processing blueprint {bp_id} (i={bn} of {n_bps})')

        i = 0
        this_date = start_date
        while this_date >= inclusive_end_date:

            if i % 50 == 0:
                print('Running for date {0} (i={1})'.format(this_date, i))
            i += 1

            profits = []
            profit_margins = []
            profit_percentages = []
            cost_prices = []
            recipe_names = []
            bp_ids = []
            bp_prod_ids = []
            bp_prod_group_ids = []
            group_names = []

            recipe = get_recipe_from_blueprint(RUNS, bp_id, price_mode=PRICE_MODE, average_mode=True, date=this_date)
            if recipe is None:
                this_date = this_date + timedelta(days=-1)
                continue

            if recipe.has_data_on_date() is False:
                logging.warning('WARN: One or more items for {0} has no average price data for date {1}'
                                .format(recipe.name, this_date))
                this_date = this_date + timedelta(days=-1)
                continue

            profits.append((recipe.name, recipe.profit(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)))
            profit_margins.append((recipe.name, recipe.profit_margin(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                         FACILITY_TAX_RATE)))
            profit_percentages.append((recipe.name, recipe.profit_percentage(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                         FACILITY_TAX_RATE)))
            cost_prices.append((recipe.name, recipe.cost_price(FULL_SALES_TAX)))
            recipe_names.append(recipe.name)
            bp_ids.append(bp_id)
            bp_prod_id = get_blueprint_product(bp_id)
            bp_prod_ids.append(bp_prod_id)
            bp_group = get_group(bp_prod_id)
            bp_prod_group_ids.append(bp_group)
            group_names.append(get_group_name(bp_group))

            if SAVE_RESULTS:

                def _fv(val):
                    return round(val[1], 2)

                for p, pm, pp, cp, name, bp_id, product_id, group_id, group_name in zip(profits, profit_margins,
                                                                                        profit_percentages,
                                                                                        cost_prices, recipe_names,
                                                                                        bp_ids, bp_prod_ids,
                                                                                        bp_prod_group_ids, group_names):



                    csv_saver.add_data([this_date, name,
                                       _fv(p), _fv(pm), _fv(pp), _fv(cp),
                                       RUNS, SYSTEM_COST_INDEX, FACILITY_TAX_RATE, FULL_SALES_TAX,
                                       PRICE_MODE, bp_id, product_id, group_id, group_name])

            this_date = this_date + timedelta(days=-1)


    if SAVE_RESULTS:
        csv_saver.write()
        csv_saver.finish()

    Timer.results()


if __name__ == '__main__':
    main()