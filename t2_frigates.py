#Tool for calculating margins

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.fuel_items import FUEL_GROUPS
from group_id_report import group_id_report

#-------------------------------------

GROUP_IDS = [
    #25, # t1 frigates, faction frigates, navy frigates
    831, #interceptor
    324, #assualt frigate
    830, #covert ops
    834, #stealth bomber
    893, #electronic attack ship
    1527, #logistics frigate
    1283, #expedition frigate
    ]

PRICE_MODE = 'sell'

RUNS = 1

SAVE_FILE = 'data/t2_frigates.csv'

SAVE_RESULTS = True

FUEL_ITEM_GROUPS = FUEL_GROUPS.T2_FRIGATES

MAT_EFF = 10

MAT_EFF_ROUND = False

#-------------------------------------

def main():

    group_id_report(GROUP_IDS, PRICE_MODE, RUNS, SAVE_FILE, SAVE_RESULTS, FUEL_ITEM_GROUPS,
                    mat_eff=MAT_EFF, mat_eff_round=MAT_EFF_ROUND)


if __name__ == '__main__':
    main()
