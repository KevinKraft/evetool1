#Tool for reading SDE yaml files and populating a database
#29/03/20

#To use this script, create a database in postgresql with pgadmin GUI called 'eve_db'

#-------------------------------------

import os
import logging
logging.getLogger().setLevel(logging.INFO)
import yaml
import schematics
import pickle

from src.model.metadata import TypeID, PostgresDatabaseSchema, Blueprint, GroupID, TypeDogma, DogmaAttributes
from src.metadata.util import PostgresDatabase
from src.metadata.schema import EVE_DB_SCHEMA, TYPE_ID, BLUEPRINT, GROUP_ID, TYPE_DOGMA, DOGMA_ATTRIBUTES
from src.metadata.constants import TYPE_ID_NAME_MAPPING, BLUEPRINT_NAME_MAPPINGS, GROUP_ID_NAME_MAPPINGS, \
    TYPE_DOGMA_NAME_MAPPINGS, DOGMA_ATTRIBUTES_NAME_MAPPINGS

#-------------------------------------

PATH = os.path.dirname(__file__).replace('\\','/')

SDE_PATH = PATH + 'data/SDE/fsd/'

PATH_KEY = 'path'
MODEL_KEY = 'model'
TABE_NAME_KEY = 'table_name'
MAPPING_KEY = 'name_mapping_dict'
EXCEPTIONS = 'exceptions'

TABLE_CONFIG = {
    'type': {
        PATH_KEY: SDE_PATH + 'typeIDs.yaml',
        MODEL_KEY: TypeID,
        TABE_NAME_KEY: TYPE_ID['name'],
        MAPPING_KEY: TYPE_ID_NAME_MAPPING},
    'blueprint': {
        PATH_KEY: SDE_PATH + 'blueprints.yaml',
        MODEL_KEY: Blueprint,
        TABE_NAME_KEY: BLUEPRINT['name'],
        MAPPING_KEY: BLUEPRINT_NAME_MAPPINGS,
        EXCEPTIONS: [45732]}, #exclude CCPs test blueprint
    'group': {
        PATH_KEY: SDE_PATH + 'groupIDs.yaml',
        MODEL_KEY: GroupID,
        TABE_NAME_KEY: GROUP_ID['name'],
        MAPPING_KEY: GROUP_ID_NAME_MAPPINGS},
    'type_dogma': {
        PATH_KEY: SDE_PATH + 'typeDogma.yaml',
        MODEL_KEY: TypeDogma,
        TABE_NAME_KEY: TYPE_DOGMA['name'],
        MAPPING_KEY: TYPE_DOGMA_NAME_MAPPINGS},
    'dogma_attributes': {
        PATH_KEY: SDE_PATH + 'dogmaAttributes.yaml',
        MODEL_KEY: DogmaAttributes,
        TABE_NAME_KEY: DOGMA_ATTRIBUTES['name'],
        MAPPING_KEY: DOGMA_ATTRIBUTES_NAME_MAPPINGS},
}

PICKLE_LOC = 'data/tmp_{key}_data.pickle'
LOAD_YAML_DATA = False

SAVE_TO_DATABASE = True

#-------------------------------------

def main():

    db_schema = PostgresDatabaseSchema(EVE_DB_SCHEMA)
    eve_db = PostgresDatabase(db_schema, overwrite=True)

    for key, config in TABLE_CONFIG.items():

        if LOAD_YAML_DATA or os.path.isfile(PICKLE_LOC.format(key=key)) is False:
            logging.info(f'Loading {key} data from yaml.')
            data = None
            with open(config[PATH_KEY], 'r', encoding='utf-8') as stream:
                try:
                    data = yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    raise exc
            pickle.dump(data, open(PICKLE_LOC.format(key=key), 'wb'))
        else:
            logging.info(f'Loading {key} data from pickle.')
            data = pickle.load(open(PICKLE_LOC.format(key=key), 'rb'))

        types = []
        for key_id, primitive in data.items():
            if EXCEPTIONS in config:
                if key_id in config[EXCEPTIONS]:
                    continue

            try:
                types.append(config[MODEL_KEY](key_id, primitive))
            except schematics.exceptions.DataError as e:
                print(str(e))
                continue

        if SAVE_TO_DATABASE:

            logging.info(f'Inserting {key} data into database.')
            for type_data in types:
                eve_db.insert(config[TABE_NAME_KEY], type_data.data_dict(config[MAPPING_KEY]))


    eve_db.close()


if __name__ == '__main__':
    types = main()