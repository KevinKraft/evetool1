#Tool for calculating margins for making tol components at today's date
#08/05/20

#todo: The job cost in game is 10 times smaller, but it doesn't make much of a difference. The item base price was
# only 2 mil in game, but its more like 10 mil.

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.WARNING)

from group_id_report import group_id_report

#-------------------------------------

GROUP_ID = 332

PRICE_MODE = 'buy'

RUNS = 1

SAVE_FILE = 'data/tools.csv'

SAVE_RESULTS = True

FUEL_ITEM_GROUPS = None

#-------------------------------------

def main():

    group_id_report(GROUP_ID, PRICE_MODE, RUNS, SAVE_FILE, SAVE_RESULTS, FUEL_ITEM_GROUPS)


if __name__ == '__main__':
    main()
