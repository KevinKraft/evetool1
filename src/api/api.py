#various functions to use for getting prices.
#For now it only looks in the perimeter stations and jita trade hub.

#-------------------------------------

import json
import requests
import logging
import time
import dateutil.parser
from functools import lru_cache

from src.constants.id import STRUCTURE_ID
from src.constants.id import REGION_ID
from src.util.isk_amount import ISKAmount
from src.util.diagnostic import timeit

#-------------------------------------

EVE_ESI = 'https://esi.evetech.net/latest'

STRUCTURE_IDS = [
    STRUCTURE_ID.JITA_TRADE_HUB,
    STRUCTURE_ID.PERIMETER_TRANQUILITY_T2_REFINERY, #POS, so won't work
    STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER, #POS, so won't work
]

#-------------------------------------

@lru_cache(maxsize=999)
@timeit
def make_request(uri):

    retries = 15
    response = None
    while retries > 0:
        retries -= 1
        response = requests.get(uri)
        if response.status_code != 200:
            logging.warning('Will retry. Bad response when calling {}. {}'.format(uri, response.content))
            time.sleep(1)
            continue
        else:
            break

    if retries <= 0:
        logging.warning('Max retries (15) exceeded. Bad response when calling {}. {}'.format(uri, response.content))
        return None

    return json.loads(response.content.decode('utf-8'))

@lru_cache(maxsize=999)
def get_item_orders(item_id, buy_orders=None):
    #todo: This does not show player owned station orders that have no range.

    uri = EVE_ESI+'/markets/'+str(REGION_ID.THE_FORGE)+'/orders/?type_id='+str(item_id)

    if buy_orders != None:
        order_id_filter = '&order_type='
        order_id_filter += 'buy' if buy_orders else 'sell'
        uri += order_id_filter

    res = make_request(uri)

    if res is None:
        return None

    if len(res) == 1000:
        logging.warning('Only 1000 orders returned when calling {}.'.format(uri))

    return list(filter(lambda x: x['location_id'] in STRUCTURE_IDS, res))


def get_item_buy_orders(item_id):
    return get_item_orders(item_id, buy_orders=True)


def get_item_sell_orders(item_id):
    return get_item_orders(item_id, buy_orders=False)

@lru_cache(maxsize=999)
@timeit
def get_average_price(item_id, date=None):

    avg_dict = get_average_prices(item_id)

    if avg_dict is None:
        return None

    if date == None:
        logging.warning(f'No date specified for average data of {item_id}.')
        return None

    if date in avg_dict.keys():
        return avg_dict[date]
    else:
        logging.warning('Average price for item {0} at date {1} not found.'.format(item_id, date))
        return None


@lru_cache()
@timeit
def get_average_prices(item_id):

    uri = EVE_ESI+'/markets/'+str(REGION_ID.THE_FORGE)+'/history/?type_id='+str(item_id)

    res = make_request(uri)

    if res is None:
        return None

    averages = sorted(res, key=lambda x: x['date'], reverse=True)

    if len(averages) == 0:
        logging.error(f'No average pricing data found for item {item_id}.')
        return None

    avg_dict = {}
    for av in averages:
        avg_dict[dateutil.parser.parse(av['date']).date()] = av['average']

    return avg_dict

# #old
# @lru_cache(maxsize=999)
# @timeit
# def get_average_price(item_id, date=None):
#
#     uri = EVE_ESI+'/markets/'+str(REGION_ID.THE_FORGE)+'/history/?type_id='+str(item_id)
#
#     res = make_request(uri)
#
#     averages = sorted(res, key=lambda x: x['date'], reverse=True)
#
#     if len(averages) == 0:
#         logging.error(f'No average pricing data found for item {item_id}.')
#         return None
#
#     if date == None:
#         av = averages[0]
#         logging.warning('Returning average price data from date {0} as None was specified.'.format(av['date']))
#         return av['average']
#
#     for av in averages:
#         if dateutil.parser.parse(av['date']).date() == date:
#             return av['average']
#
#     logging.warning('Average price for item {0} at date {1} not found.'.format(item_id, date))
#     return None


#-------------------------------------

def _get_items_for_order_on_page(page):

    uri = EVE_ESI+'/markets/'+str(REGION_ID.THE_FORGE)+'/types/?page='+str(page)

    while True:
        response = requests.get(uri)
        if response.status_code != 200:
            logging.warning('Will retry. Bad response when calling {}. {}'.format(uri, response.content))
            time.sleep(1)
            continue
        else:
            break

    res = json.loads(response.content.decode('utf-8'))
    return res

def get_items_for_order():
    #todo: This does not show player owned station orders that have no range.
    page = 1
    item_ids = []
    page_item_ids = [0]
    while len(page_item_ids) != 0:
        page_item_ids = _get_items_for_order_on_page(page)
        page += 1
        item_ids += page_item_ids

    return item_ids

#-------------------------------------

@timeit
def get_min_sell_price(item_id, min_amount=1):
    orders = get_item_sell_orders(item_id)
    orders = list(filter(lambda x: x['volume_remain'] >= min_amount, orders))
    orders = sorted(orders, key=lambda x: x['price'])
    if len(orders) == 0:
        logging.warning('No matching orders for item_id {}.'.format(item_id))
        return None
    return ISKAmount(orders[0]['price'])

@timeit
def get_max_buy_price(item_id):
    max_order = get_max_buy_order(item_id)
    if max_order is None:
        return None
    return ISKAmount(max_order['price'])


def get_max_buy_order(item_id):
    orders = get_item_buy_orders(item_id)
    orders = sorted(orders, key=lambda x: x['price'], reverse=True)

    if len(orders) == 0:
        logging.warning('No matching buy orders for item_id {}.'.format(item_id))
        return None
    return orders[0]


def buy_sell_profit(item_id):
    sp = get_min_sell_price(item_id)
    b_order = get_max_buy_order(item_id)
    #dont worry about the tax for now
    if sp != None and b_order != None:
        bp = ISKAmount(b_order['price'])
        return bp - sp
    else:
        return None
