import logging
from functools import lru_cache

from src.model.metadata import PostgresDatabaseSchema, BlueprintBaseActivity
from src.util.diagnostic import timeit

from src.metadata.util import PostgresDatabase
from src.metadata.schema import EVE_DB_SCHEMA, TYPE_ID, GROUP_ID, BLUEPRINT

#-------------------------------------

SCHEMA = PostgresDatabaseSchema(EVE_DB_SCHEMA)
DATABASE = PostgresDatabase(SCHEMA)

#-------------------------------------

class DatabaseError(Exception):
    pass

#-------------------------------------

@lru_cache(maxsize=5)
def get_all_blueprints():
    tname = BLUEPRINT['name']
    results = DATABASE.read_table(tname, ['id'])

    if len(results) == 0:
        logging.warning(f'No {tname} entries found.')
        return None
    else:
        return results


@lru_cache(maxsize=999)
def get_all_types(group_id: int):
    tname = TYPE_ID['name']
    results = DATABASE.read_table(tname, ['id'], filter_map={'groupid': group_id})

    if len(results) == 0:
        logging.warning(f'No {tname} entry for given group_id ({group_id}).')
        return None
    else:
        return results


@lru_cache(maxsize=5)
def get_all_types_from_database():
    tname = TYPE_ID['name']
    results = DATABASE.read_table(tname, ['id'])

    if len(results) == 0:
        logging.warning(f'No {tname} entries found.')
        return None
    else:
        return results


@lru_cache(maxsize=999)
def get_blueprint(type_id: int):
    tname = BLUEPRINT['name']
    results = DATABASE.read_table(tname, ['id'], filter_map={'productid': type_id})

    if len(results) == 0:
        logging.warning(f'No {tname} entry for given type_id ({type_id}).')
        return None
    elif len(results) > 1:
        logging.warning(f'More than one {tname} entry found for given type_id ({type_id}).')
        return None
    else:
        return results[0]


@lru_cache(maxsize=999)
@timeit
def get_blueprint_product(bp_id: int):
    tname = BLUEPRINT['name']
    results = DATABASE.read_table(tname, ['productid'], filter_map={'id': bp_id})

    if len(results) == 0:
        logging.warning(f'No {tname} entry for given type_id ({bp_id}).')
        return None
    elif len(results) > 1:
        logging.warning(f'More than one {tname} entry found for given bp_id ({bp_id}).')
        return None
    else:
        return results[0]

@lru_cache(maxsize=999)
@timeit
def get_group(type_id: int):
    results = DATABASE.read_table(TYPE_ID['name'], ['groupid'], filter_map={'id': type_id})

    if len(results) == 0:
        logging.warning(f'No type_id entry for given type_id ({type_id}).')
        return None
    elif len(results) > 1:
        logging.warning(f'More than one type_id entry found for given type_id ({type_id}).')
        return None
    else:
        return results[0]


@lru_cache(maxsize=999)
@timeit
def get_group_name(group_id: int):
    tname = GROUP_ID['name']
    results = DATABASE.read_table(tname, ['name'], filter_map={'id': group_id})

    if len(results) == 0:
        logging.warning(f'No {tname} entry for given group_id ({group_id}).')
        return None
    elif len(results) > 1:
        logging.warning(f'More than one {tname} entry found for given group_id ({group_id}).')
        return None
    else:
        return results[0]


@lru_cache(maxsize=999)
@timeit
def get_name(type_id: int):
    results = DATABASE.read_table(TYPE_ID['name'], ['name'], filter_map={'id': type_id})

    if len(results) == 0:
        logging.warning(f'No type_id entry for given type_id ({type_id}).')
        return None
    elif len(results) > 1:
        logging.warning(f'More than one type_id entry found for given type_id ({type_id}).')
        return None
    else:
        return results[0]


def get_recipe(type_id: int):
    bp = get_blueprint(type_id)
    if bp is None:
        return None
    return get_recipe_from_bp(bp)


@lru_cache(maxsize=999)
@timeit
def get_recipe_from_bp(bp_id: int):
    tname = BLUEPRINT['name']
    mres = DATABASE.read_table(tname, ['manufacturing'], filter_map={'id': bp_id})
    rres = DATABASE.read_table(tname, ['reaction'], filter_map={'id': bp_id})

    nm = sum([1 for d in mres if d != {}])
    nr = sum([1 for d in rres if d != {}])
    if nm == 0 and nr == 0:
        logging.warning(f'No {tname} entry for given bp_id ({bp_id}).')
        return None
    elif nm > 0 and nr > 0:
        raise DatabaseError(f'Both a manufacturing and a reaction entry were found for the given bp_id ({bp_id}).')
    elif nm > 1 or nr > 1:
        raise DatabaseError(f'More than one of recipe was found for the given bp_id ({bp_id}).')
    elif nm == 1:
        return BlueprintBaseActivity(mres[0])
    elif nr == 1:
        return BlueprintBaseActivity(rres[0])
    else:
        raise DatabaseError(f'Unknown problem getting recipe from {tname} for given bp_id ({bp_id}).')



@lru_cache(maxsize=999)
def get_type_from_name(name: str):
    results = DATABASE.read_table(TYPE_ID['name'], ['id'], filter_map={'name': name})

    if len(results) == 0:
        logging.warning(f'No type_id entry for given name ({name}).')
        return None
    elif len(results) > 1:
        logging.warning(f'More than one type_id entry found for given name ({name}).')
        return None
    else:
        return results[0]


@lru_cache(maxsize=999)
@timeit
def get_volume(type_id: int):
    tname = TYPE_ID['name']
    results = DATABASE.read_table(tname, ['volume'], filter_map={'id': type_id})

    if len(results) == 0:
        logging.warning(f'No {tname} entry for given type_id ({type_id}).')
        return None
    elif len(results) > 1:
        logging.warning(f'More than one {tname} entry found for given type_id ({type_id}).')
        return None
    else:
        return results[0]


def is_blueprint_reaction(bp_id: int):
    return _is_reaction(bp_id)


def is_reaction(type_id: int):
    bp_id = get_blueprint(type_id)
    return _is_reaction(bp_id)


def _is_reaction(bp_id: int):
    tname = BLUEPRINT['name']
    rres = DATABASE.read_table(tname, ['reaction'], filter_map={'id': bp_id})
    nr = sum([1 for d in rres if d != {}])
    return nr == 1
