#utils for postgresql database
#29/03/20

#-------------------------------------

import logging
import psycopg2
from typing import Dict, List, Union
import json

from src.model.metadata import PostgresDatabaseSchema, PostgresTableSchema

#-------------------------------------

POSTGRES_USER = 'postgres'
POSTGRES_PASSWORD = 'password'
POSTGRES_HOST = '127.0.0.1'
POSTGRES_PORT = '5432'

#-------------------------------------

class PostgresConnection(object):

    def __init__(self, database: str):
        self._database = database
        self._cursor, self._connection = self._connect()

    @property
    def database(self):
        return self._database

    def close(self):
        if (self._connection):
            self._cursor.close()
            self._connection.close()
            logging.info(f"PostgreSQL connection to {self._database} is closed")

    def execute(self, cmd):
        try:
            self._cursor.execute(cmd)
            self._connection.commit()
            if self._cursor.description is not None:
                ret = self._cursor.fetchone()
            else:
                ret = None
        except (Exception, psycopg2.Error) as error:
            logging.error(f"Error while running command {cmd}.")
            raise error
        return ret

    def execute_temp(self, cmd):
        try:
            self._cursor.execute(cmd)
            self._connection.commit()
            if self._cursor.description is not None:
                ret = self._cursor.fetchall()
            else:
                ret = None
        except (Exception, psycopg2.Error) as error:
            logging.error(f"Error while running command {cmd}.")
            raise error
        return ret

    def _connect(self):
        try:
            connection = psycopg2.connect(user=POSTGRES_USER,
                                          password=POSTGRES_PASSWORD,
                                          host=POSTGRES_HOST,
                                          port=POSTGRES_PORT,
                                          database=self._database)

            cursor = connection.cursor()
            logging.info(connection.get_dsn_parameters())

            cursor.execute("SELECT version();")
            record = cursor.fetchone()
            logging.info("You are connected to - " + str(record))

            return cursor, connection

        except (Exception, psycopg2.Error) as error:
            logging.error("Error while connecting to PostgreSQL " + str(error))
            raise error


class PostgresDatabase(PostgresConnection):

    def __init__(self, schema: PostgresDatabaseSchema = None, overwrite: bool = False):
        super(PostgresDatabase, self).__init__(schema.name)
        self._schema = schema or {}
        if overwrite:
            self._create_tables()

    def create_table(self, schema: PostgresTableSchema = None):
        self._create_table(schema)
        self._schema.tables.append(schema)

    def drop_table(self, table_name):
        if not self.has_table(table_name):
            raise ValueError(f'Database {self.database} does not have table {table_name}')
        cmd = f'DROP TABLE {table_name}'
        self.execute(cmd)

    def has_table(self, table_name):
        ret = self.execute_temp(f"SELECT EXISTS(SELECT * FROM information_schema.tables WHERE table_name='{table_name}')")
        return ret[0][0]

    def insert(self, table_name, data: Dict):
        table_schema = self._get_table_schema(table_name)
        col_names = table_schema.keys()
        if set(data.keys()) != set(col_names):
            raise AttributeError(f'Given data dict column names ({data.keys}) do not match table schema '
                                 f'({col_names}).')
        spc = ' '
        cmd = f'INSERT INTO {table_name} '
        cols_str = '('
        vals_str = '('
        sep = ','
        ncols = len(col_names)
        for i, col in enumerate(col_names):
            col_type = table_schema.get_column(col).type
            cols_str += col
            vals_str += self._sanitise(data[col], col_type)
            if i < ncols-1:
                cols_str += sep + spc
                vals_str += sep + spc
        cols_str += ')'
        vals_str += ')'

        cmd += cols_str + ' VALUES ' + vals_str
        self.execute(cmd)

    def read_table(self, table: str, columns: List[str], filter_map: Dict[str, Union[str, int]] = None):
        columns = [self._sanitise_col(c) for c in columns]
        cols_str = ','.join(columns)
        cmd = f'SELECT {cols_str} from {table}'

        tschema = self._get_table_schema(table)
        if not filter_map is None:
            for key, value in filter_map.items():
                col_type = tschema.get_column(key).type
                col = self._sanitise_col(key)
                value = self._sanitise(value, col_type)
                cmd += f' WHERE {col} = {value}'

        return [v[0] for v in self.execute_temp(cmd)]

    def _create_table(self, schema: PostgresTableSchema = None):
        table_name = schema.name

        if self.has_table(table_name):
            self.drop_table(table_name)

        if table_name in self._schema.keys():
            if schema != self._schema[table_name]:
                raise ValueError('Table already exists and given schema is different to expected schema.')
        primary_key = schema.primary_key
        cmd = 'CREATE TABLE IF NOT EXISTS ' + str(table_name) + ' ('
        spc = ' '
        ncols = len(schema.columns)
        for i, col in enumerate(schema.columns):
            if col.name == primary_key:
                pkey = 'PRIMARY KEY' + spc
            else:
                pkey = ''
            if col.nullable is True:
                nullable = ''
            else:
                nullable = 'NOT NULL'
            cmd += spc + col.name + spc + col.type + spc + pkey + nullable
            if i < ncols-1:
                cmd += ','

        cmd += ');'
        self.execute(cmd)

    def _create_tables(self):
        for table_schema in self._schema.tables:
            self._create_table(table_schema)

    def _get_table_schema(self, table) -> PostgresTableSchema:
        tschema = self._schema.get_table_schema(table)
        if tschema is None:
            raise AttributeError(f'Database schema {self.database} does not have schema for table {table}.')
        return tschema

    def _sanitise(self, data, col_type):
        if data is None:
            if col_type == 'INT' or 'REAL':
                data = str(-1)
            elif col_type == 'TEXT':
                data = ''
        if col_type == 'INT' or col_type == 'REAL':
            return str(data)
        elif col_type == 'TEXT':
            qu = "'"
            return qu + str(data).replace("'", "''") + qu
        elif col_type == 'JSON':
            qu = "'"
            return qu + str(json.dumps(data)) + qu
        else:
            raise AttributeError(f'Unknown SQL type {col_type}.')

    def _sanitise_col(self, col_name):
        qc = '"'
        return qc + col_name + qc