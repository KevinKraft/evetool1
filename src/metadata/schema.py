#schemas for data stored in postgres tables
#13/04/20

#-------------------------------------

TYPE_ID = {
    'name': 'type_id',
    'columns': [
        {'name': 'id', 'type': 'INT', 'is_primary': True},
        {'name': 'groupid', 'type': 'INT'},
        {'name': 'name', 'type': 'TEXT'},
        {'name': 'volume', 'type': 'REAL'},
    ]}


BLUEPRINT = {
    'name': 'blueprint',
    'columns': [
        {'name': 'id', 'type': 'INT', 'is_primary': True},
        {'name': 'bpid', 'type': 'INT'},
        {'name': 'productid', 'type': 'INT'},
        {'name': 'manufacturing', 'type': 'JSON'},
        {'name': 'reaction', 'type': 'JSON'},
    ]}


GROUP_ID = {
    'name': 'group_id',
    'columns': [
        {'name': 'id', 'type': 'INT', 'is_primary': True},
        {'name': 'cid', 'type': 'INT'},
        {'name': 'name', 'type': 'TEXT'},
    ]}



TYPE_DOGMA = {
    'name': 'type_dogma',
    'columns': [
        {'name': 'id', 'type': 'INT', 'is_primary': True},
        {'name': 'dogma_attributes', 'type': 'JSON'},
    ]}


DOGMA_ATTRIBUTES = {
    'name': 'dogma_attributes',
    'columns': [
        {'name': 'id', 'type': 'INT', 'is_primary': True},
        {'name': 'name', 'type': 'TEXT'},
    ]}


EVE_DB_SCHEMA = {
    'name': 'eve_db',
    'tables': [
        TYPE_ID,
        BLUEPRINT,
        GROUP_ID,
        TYPE_DOGMA,
        DOGMA_ATTRIBUTES,
    ]}
