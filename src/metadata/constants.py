#constants for postgresql database
#29/03/20

#-------------------------------------
# Yaml/Model to db name mappings
#-------------------------------------

TYPE_ID_NAME_MAPPING = {
    'id' : 'id',
    'groupID': 'groupid',
    'name_en': 'name',
    'volume': 'volume',
}


BLUEPRINT_NAME_MAPPINGS = {
    'id' : 'id',
    'blueprintTypeID' : 'bpid',
    'product_id' : 'productid',
    'manufacturing': 'manufacturing',
    'reaction': 'reaction'
}


GROUP_ID_NAME_MAPPINGS = {
    'id' : 'id',
    'categoryID' : 'cid',
    'name_en': 'name'
}


TYPE_DOGMA_NAME_MAPPINGS = {
    'id' : 'id',
    'dogma_attributes' : 'dogma_attributes',
}


DOGMA_ATTRIBUTES_NAME_MAPPINGS = {
    'id' : 'id',
    'name' : 'name',
}
