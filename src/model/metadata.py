#models for postgresql database
#29/03/20

#-------------------------------------

import logging
from schematics.models import Model
from schematics.types import IntType, FloatType, DictType, StringType, BooleanType, BaseType, ListType, ModelType, \
    UnionType
from typing import Dict, Union
import json

#-------------------------------------
# Yaml metadata models
#-------------------------------------

class KeyIDModel(Model):

    def __init__(self, key: str, data: Dict):
        data['id'] = key
        super(KeyIDModel, self).__init__(data)

    def data_dict(self, name_map: Dict[str, str]) -> Dict[str, str]:
        data = {}
        for key, value in name_map.items():
            data[value] = self.__getattribute__(key)
        return data


class TypeID(KeyIDModel):
    id = IntType(required=True)
    groupID = IntType(required=True)
    mass = FloatType(required=False)
    name = DictType(StringType, required=True)
    portionSize = IntType(required=False)
    published = BooleanType(required=False)
    volume = FloatType(required=False)
    description = DictType(StringType, required=False)

    radius = FloatType(default=None)
    soundID = IntType(default=None)
    graphicID = IntType(default=None)
    iconID = IntType(default=None)
    raceID = IntType(default=None)
    sofFactionName = StringType(default=None)
    marketGroupID = IntType(default=None)
    basePrice = FloatType(default=None)
    capacity = FloatType(default=None)
    metaGroupID = IntType(default=None)
    variationParentTypeID = IntType(default=None)
    masteries = BaseType(default=None)
    traits = BaseType(default=None)
    factionID = IntType(default=None)
    sofMaterialSetID = IntType(default=None)

    @property
    def name_en(self):
        if 'en' in self.name:
            return self.name['en']
        else:
            return str(self.id)


class BlueprintManufacturingMaterial(Model):
    quantity = IntType(required=True)
    typeID = IntType(required=True)


class BlueprintManufacturingProduct(BlueprintManufacturingMaterial):
    quantity = IntType(required=True)
    typeID = IntType(required=True)
    probability = FloatType()

    @property
    def product_id(self):
        return self.typeID


class BlueprintSkill(Model):
    level = IntType(required=True)
    typeID = IntType(required=True)


class BlueprintBaseActivity(Model):
    time = IntType(required=True)
    materials = ListType(ModelType(BlueprintManufacturingMaterial))
    products = ListType(ModelType(BlueprintManufacturingProduct))
    skills = ListType(ModelType(BlueprintSkill))

    @property
    def product_id(self):
        pd = self.products
        if pd:
            #There are no bps with more than one product
            return pd[0].product_id
        else:
            return -1

class BlueprintActivities(Model):
    copying = ModelType(BlueprintBaseActivity, required=True)
    manufacturing = ModelType(BlueprintBaseActivity, required=False)
    research_material = ModelType(BlueprintBaseActivity, required=True)
    research_time = ModelType(BlueprintBaseActivity, required=True)
    invention = ModelType(BlueprintBaseActivity, required=False)
    reaction = ModelType(BlueprintBaseActivity, required=False)


class Blueprint(KeyIDModel):
    id = IntType(required=True)
    activities = ModelType(BlueprintActivities, required=True)
    blueprintTypeID = IntType(required=True)
    maxProductionLimit = IntType(required=True)

    @property
    def manufacturing(self):
        mf = self.activities.manufacturing
        if mf is None:
            return {}
        else:
            return mf.to_primitive()

    @property
    def product_id(self):
        mf = self.activities.manufacturing
        rc = self.activities.reaction
        mt = mf or rc
        if mt:
            return mt.product_id
        return -1

    @property
    def reaction(self):
        rc = self.activities.reaction
        if rc is None:
            return {}
        else:
            return rc.to_primitive()


class GroupID(KeyIDModel):
    id = IntType(required=True)
    name = DictType(StringType, required=True)
    categoryID = IntType(required=True)

    anchorable = BooleanType(required=False)
    anchored = BooleanType(required=False)
    fittableNonSingleton = BooleanType(required=False)
    published = BooleanType(required=False)
    useBasePrice = BooleanType(required=False)
    iconID = IntType(required=False)

    @property
    def name_en(self):
        if 'en' in self.name:
            return self.name['en']
        else:
            return str(self.id)


class DogmaAttribute(Model):
    attributeID = IntType(required=True)
    value = UnionType((FloatType, IntType), required=True)


class DogmaEffect(Model):
    effectID = IntType(required=False)
    isDefault = BooleanType(required=False)


class TypeDogma(KeyIDModel):
    id = IntType(required=True)
    dogmaAttributes = ListType(ModelType(DogmaAttribute), required=True)
    dogmaEffects = ListType(ModelType(DogmaEffect), required=False)

    @property
    def dogma_attributes(self):
        return [m.to_primitive() for m in self.dogmaAttributes]


class DogmaAttributes(KeyIDModel):
    id = IntType(required=True)
    attributeID = IntType(required=False)
    categoryID = IntType(required=False)
    dataType = StringType(required=False)
    defaultValue = FloatType(required=False)
    description = StringType(required=False)
    displayNameID = BaseType(required=False)
    highIsGood = BooleanType(required=False)
    iconID = IntType(required=False)
    name = StringType(required=True)
    published = BooleanType(required=False)
    stackable = BooleanType(required=False)
    tooltipDescriptionID = BaseType(required=False)
    tooltipTitleID = BaseType(required=False)
    unitID = IntType(required=False)
    maxAttributeID = IntType(required=False)
    chargeRechargeTimeID = IntType(required=False)

#-------------------------------------
# Database schema models
#-------------------------------------

class PostgresColumnSchema(Model):
    name = StringType(required=True)
    type = StringType(required=True)
    nullable = BooleanType(required=False, default=False)
    is_primary = BooleanType(required=False, default=False)


class PostgresTableSchema(Model):
    name = StringType(required=True)
    columns = ListType(ModelType(PostgresColumnSchema), required=True)

    @property
    def primary_key(self):
        return self._get_primary_key()

    def validate(self, partial=False, convert=True, app_data=None, **kwargs):
        super(PostgresTableSchema, self).validate(partial=partial, convert=convert, app_data=app_data, **kwargs)
        self._get_primary_key()

    def get_column(self, col_name):
        for col in self.columns:
            if col.name == col_name:
                return col
        raise AttributeError(f'Table {self.name} has no column {col_name}.')

    def keys(self):
        keys = []
        for col in self.columns:
            keys.append(col.name)
        return keys

    def _get_primary_key(self):
        primary = None
        for col in self.columns:
            if col.is_primary:
                if primary is None:
                    primary = col.name
                else:
                    raise AttributeError(f'Table {self.name} has more than one primary key.')
        return primary


class PostgresDatabaseSchema(Model):
    name = StringType(required=True)
    tables = ListType(ModelType(PostgresTableSchema), required=True)

    def get_table_schema(self, table_name) -> Union[PostgresTableSchema, None]:
        for tab in self.tables:
            if tab.name == table_name:
                return tab
        logging.warning(f'Database schema {self.name} does not have schema for table {table_name}.')
        return None