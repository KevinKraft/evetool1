import os
import pandas
import logging
from io import StringIO, SEEK_END
from csv import writer
import shutil

from src.util.diagnostic import timeit

#-------------------------------------

class SaveRowAsCsv(object):
    '''
    Append a row of data into a given csv file.
    '''
    @timeit
    def __init__(self, file_path, headers=None):
        self._file_path = file_path
        if not os.path.isfile(self._file_path):
            logging.warning(f'Given file ({file_path}) does not exist, creating it.')
            self._df = pandas.DataFrame(columns=headers)
            self.write()

        self._df = pandas.read_csv(self._file_path, delimiter=',', encoding='utf-8')

        self._check_headers(headers)

        self._nrows = len(self._df)

    @property
    def headers(self):
        return list(self._df.columns.values)

    def _check_headers(self, headers):
        obj_headers = self.headers
        assert headers == obj_headers, f'Given headers ({str(headers)}) do not match headers ({str(obj_headers)}).'

    @timeit
    def write(self):
        self._df.to_csv(self._file_path, sep=',', index=False, encoding='utf-8')

    @timeit
    def add_data(self, data):
        self._df.loc[self._nrows] = data
        self._nrows += 1

    @timeit
    def add_data_array(self, data_array):
        for row in data_array:
            self._df.loc[self._nrows] = row
            self._nrows += 1


class CSVIOWriter(object):

    @timeit
    def __init__(self, file_path, headers=None, new_file=True):
        self._file_path = file_path
        if not os.path.isfile(self._file_path):
            logging.warning(f'Given file ({file_path}) does not exist, creating it.')

        buffer = None
        if new_file is False:
            with open(self._file_path, 'r', encoding='windows-1252') as f:
                buffer = StringIO(f.read())
                buffer.seek(0, SEEK_END)
        else:
            buffer = StringIO()

        self._buffer = buffer
        self._writer = writer(self._buffer)

        self._data_array = []
        if not headers is None and new_file is True:
            self._data_array = [headers]

    @timeit
    def add_data(self, data):
        self._data_array.append(data)

    @timeit
    def write(self):
        for data in self._data_array:
            self._writer.writerow(data)
        self._data_array = []

    @timeit
    def finish(self):
        with open(self._file_path, 'w', newline='', encoding='windows-1252') as fd:
            self._buffer.seek(0)
            shutil.copyfileobj(self._buffer, fd)
