'''
The are convenience lists of group IDs for items common used as fuel items in recipes for various types of recipes.
By setting an input item to a recipe as a fuel item, it's price will always be calculated as the buy price (i.e. the
price you pat to buy it, the sell order), even if the buy mode is set to sell. This is to enable the user to
calculate the margins for selling moon goo or running a reaction and selling those products. In that case the fuel
blocks are not part of the items you would be selling, so you treat them as fuel blocks and their sell price is not
included in the margin, but the buy price is included.

'''


class FUEL_GROUPS:

    HYBRID_POLYMER = [18, 1136] #minerals, fuel blocks

    HYBRID_TECH_COMPONENT = [966] #ancient salvage

    TACTICAL_DESTROYER = [332] #tool

    STRATEGIC_CRUISER = [332] #tool

    HAC = [18, 332, 1034] #minerals, tool, pi t2

    INTERMEDIATE_MATERIAL = [1136] #fuel blocks

    COMPOSITES = [1136] #fuel blocks

    T2_FRIGATES = [18, 332, 1034] #minerals, tools, pi t2

    HEAVY_ASSAULT_CRUISER = [18, 332, 1034] #minerals, tools, pi t2

    CRUISER = [18] #minerals
