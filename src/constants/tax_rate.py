from .id import STRUCTURE_ID

#todo: This can be filled in automatically with the api and ESI auth.

TAX_RATE = {
    STRUCTURE_ID.JITA_TRADE_HUB : {'broker': 0.05, 'sales': 0.05},
    STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER: {'broker': 0.00, 'sales': 0.05},
    STRUCTURE_ID.PERIMETER_TRANQUILITY_T2_REFINERY: {'broker': 0.00, 'sales': 0.05},
}


