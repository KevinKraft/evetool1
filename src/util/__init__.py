

def standard_name(name: str):
    if name is None:
        return 'None'
    return name.replace(' ','')