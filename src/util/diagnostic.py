#timing tools for assessing the code
#16/05/20

#-------------------------------------

import time
from statistics import mean, stdev

#-------------------------------------

class TimeDiagnostics(object):

    _timings = {}
    running = False
    _start_time = time.time()

    def add_time(self, fname, time):

        if not self.running:
            return

        if not fname in self._timings:
            self._timings[fname] = []

        self._timings[fname].append(time)

    def results(self):

        print('-'*80)
        print('Timing results:')
        print('-'*80)

        for key, times in self._timings.items():

            times = [time/1000 for time in times]

            x = mean(times)
            n = len(times)
            if n > 2:
                s = round(stdev(times, x), 4)
            else:
                s = None
            t = round(sum(times), 4)


            kstr = (key+': ').ljust(30, ' ')
            xstr = f'mean: {round(x,4)}, '.ljust(15, ' ')
            sstr = f'stdev: {s}, '.ljust(15, ' ')
            nstr = f'calls: {n}'.ljust(15, ' ')
            tstr = f'total: {t}'.ljust(15, ' ')

            print(kstr + xstr + sstr + nstr + tstr)

        print('-'*80)
        ttime = int(time.time() - self._start_time)

        print(f'Total time: {round(ttime, 2)}')
        print('-'*80)


Timer = TimeDiagnostics()


def timeit(method):
    def timed(*args, **kw):


        if not Timer.running:
            return method(*args, **kw)

        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        name = method.__name__
        if name == '__init__':
            smeth = str(method)
            name = smeth[10: smeth.find('__init__') + 8]

        Timer.add_time(name, int((te - ts) * 1000))

        return result
    return timed