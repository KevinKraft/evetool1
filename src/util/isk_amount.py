#class for dealing with values in ISK and displaying larger numbers

class ISKAmount(float):

    ONE_THOUSAND = 1000
    ONE_MILLION = 1000000
    ONE_BILLION = 1000000000

    def __init__(self, number):
        self._round = 2
        self._number = round(number, self._round)
        super(ISKAmount, self).__init__()

    def __add__(self, other):
        return ISKAmount(super().__add__(other))

    def __mul__(self, other):
        return ISKAmount(super().__mul__(other))

    def __sub__(self, other):
        return ISKAmount(super().__sub__(other))


    def __radd__(self, other):
        return ISKAmount(super().__radd__(other))


    def __rmul__(self, other):
        return ISKAmount(super().__rmul__(other))

    def __rsub__(self, other):
        return ISKAmount(super().__rsub__(other))

    def __str__(self):
        return self._print(self._number, self._round)

    def __eq__(self, other):
        if isinstance(other, ISKAmount):
            return other._number == self._number
        else:
            return other == self._number

    @property
    def bil(self, round_=None):
        return self._display_number(self.ONE_BILLION, round_, ' bil')

    @property
    def mil(self, round_=None):
        return self._display_number(self.ONE_MILLION, round_, ' mil')

    @property
    def thousands(self, round_=None):
        return self._display_number(self.ONE_THOUSAND, round_, ' k')

    def _display_number(self, denominator = 1, round_=None, extra=''):
        num =  self._number / denominator + 0.0
        return self._print(num, round_, extra)

    def _print(self, value, round_=None, extra=''):
        return '{:,}'.format(round(value, round_ or self._round)) + extra + ' ISK'