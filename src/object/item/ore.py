#-------------------------------------

from math import floor

from src.constants.id import ITEM_ID

from src.object.recipe.reprocessable_item import ReprocessableItem
from src.util.isk_amount import ISKAmount
from src.object.item.mineral import Tritanium

#-------------------------------------

class OreItem(ReprocessableItem):

    def reprocessed_materials(self, reprocessing_rate):
        loads = floor(self.amount / self._reprocess_load + 0.0)
        perfect = self._reprocessed_materials.amount * loads
        amount = (perfect * reprocessing_rate)
        return self._reprocessed_materials.__class__(amount)

    def reprocessing_fee(self, tax_rate):
        # todo: In the game this is either floored or rounded with .5 goes to .0, not sure which.
        return ISKAmount(self.buy_price * tax_rate)


class Veldspar(OreItem):
    _name = 'Veldspar'
    _item_id = ITEM_ID.VELDSPAR
    _unit_volume = 0.1
    _reprocessed_materials = Tritanium(415)
    _reprocess_load = 100


class StableVeldspar(Veldspar):
    _name = 'Stable Veldspar'
    _item_id = ITEM_ID.STABLE_VELDSPAR
    _reprocessed_materials = Tritanium(477)


class DenseVeldspar(Veldspar):
    _name = 'Dense Veldspar'
    _item_id = ITEM_ID.DENSE_VELDSPAR
    _reprocessed_materials = Tritanium(457)


class ConcentratedVeldspar(Veldspar):
    _name = 'Concentrated Veldspar'
    _item_id = ITEM_ID.CONCENTRATED_VELDSPAR
    _reprocessed_materials = Tritanium(436)


class CompressedVeldspar(Veldspar):
    _name = 'Compressed Veldspar'
    _item_id = ITEM_ID.COMPRESSED_VELDSPAR
    _unit_volume = 0.15
    _reprocess_load = 1


class CompressedStableVeldspar(StableVeldspar):
    _name = 'Compressed Stable Veldspar'
    _item_id = ITEM_ID.COMPRESSED_STABLE_VELDSPAR
    _unit_volume = 0.15
    _reprocess_load = 1


class CompressedDenseVeldspar(DenseVeldspar):
    _name = 'Compressed Dense Veldspar'
    _item_id = ITEM_ID.COMPRESSED_DENSE_VELDSPAR
    _unit_volume = 0.15
    _reprocess_load = 1


class CompressedConcentratedVeldspar(ConcentratedVeldspar):
    _name = 'Compressed Concentrated Veldspar'
    _item_id = ITEM_ID.COMPRESSED_CONCENTRATED_VELDSPAR
    _unit_volume = 0.15
    _reprocess_load = 1

ORE_ITEMS = [OreItem,
             Veldspar, StableVeldspar, DenseVeldspar, ConcentratedVeldspar,
             CompressedVeldspar, CompressedStableVeldspar, CompressedDenseVeldspar, CompressedConcentratedVeldspar]