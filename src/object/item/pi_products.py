from src.constants.id import ITEM_ID

from src.object.item.eve_item import EveItem

#-------------------------------------

class PIProduct(EveItem):
    _unit_volume = 0.01


class Robotics(PIProduct):

    _name = 'Robotics'
    _item_id = ITEM_ID.ROBOTICS




PI_PRODUCTS_ITEMS = [Robotics]