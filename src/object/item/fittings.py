#classes for information about gas items in eve and stacks of items and their values

#todo: Much of this can be populated from the API and using python schematics.

#-------------------------------------

from src.constants.id import ITEM_ID

from src.object.item.eve_item import EveItem

#-------------------------------------

class Fitting(EveItem):
    pass

class N125MMRailgunI(Fitting):
    _name = '125mm Railgun I'
    _item_id = ITEM_ID.N125MM_RAILGUN_I
    _unit_volume =5.


class N125MMRailgunII(Fitting):
    _name = '125mm Railgun II'
    _item_id = ITEM_ID.N125MM_RAILGUN_II
    _unit_volume =5.


FITTING_ITEMS = [N125MMRailgunI]