#-------------------------------------

from src.constants.id import ITEM_ID

from src.object.item.eve_item import EveItem

#-------------------------------------

class MineralItem(EveItem):
    _unit_volume = 0.01


class Tritanium(MineralItem):

    _name = 'Tritanium'
    _item_id = ITEM_ID.TRITANIUM


class Pyerite(MineralItem):

    _name = 'Pyerite'
    _item_id = ITEM_ID.PYERITE


class Mexallon(MineralItem):

    _name = 'Mexallon'
    _item_id = ITEM_ID.MEXALLON


class Isogen(MineralItem):

    _name = 'Isogen'
    _item_id = ITEM_ID.ISOGEN


class Nocxium(MineralItem):

    _name = 'Nocxium'
    _item_id = ITEM_ID.NOCXIUM


class Zydrine(MineralItem):
    _name = 'Zydrine'
    _item_id = ITEM_ID.ZYDRINE


class Megacyte(MineralItem):

    _name = 'Megacyte'
    _item_id = ITEM_ID.MEGACYTE


class Morphite(MineralItem):

    _name = 'Morphite'
    _item_id = ITEM_ID.MORPHITE


MINERAL_ITEMS = [MineralItem, Tritanium, Pyerite, Mexallon, Isogen, Nocxium, Zydrine, Megacyte, Morphite]