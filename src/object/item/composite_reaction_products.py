from src.constants.id import ITEM_ID

from src.object.recipe.reprocessable_item import ReprocessableItem

#-------------------------------------

class CompositeReactionProduct(ReprocessableItem):
    pass


class CaesariumCadmide(CompositeReactionProduct):
    _name = 'Caesarium Cadmide'
    _item_id = ITEM_ID.CAESARIUM_CADMIDE
    _unit_volume = 0.2


class CarbonPolymers(CompositeReactionProduct):
    _name = 'Carbon Polymers'
    _item_id = ITEM_ID.CARBON_POLYMERS
    _unit_volume = 0.2


class CeramicPowder(CompositeReactionProduct):
    _name = 'Ceramic Powder'
    _item_id = ITEM_ID.CERAMIC_POWDER
    _unit_volume = 0.2


class CrystalliteAlloy(CompositeReactionProduct):
    _name = 'Crystallite Alloy'
    _item_id = ITEM_ID.CRYSTALLITE_ALLOY
    _unit_volume = 0.2


class Dysporite(CompositeReactionProduct):
    _name = 'Dysporite'
    _item_id = ITEM_ID.DYSPORITE
    _unit_volume = 0.2


class FerniteAlloy(CompositeReactionProduct):
    _name = 'Fernite Alloy'
    _item_id = ITEM_ID.FERNITE_ALLOY
    _unit_volume = 0.2


class Ferrofluid(CompositeReactionProduct):
    _name = 'Ferrofluid'
    _item_id = ITEM_ID.FERROFLUID
    _unit_volume = 0.2


class FluxedCondensates(CompositeReactionProduct):
    _name = 'Fluxed Condensates'
    _item_id = ITEM_ID.FLUXED_CONDENSATES
    _unit_volume = 0.2


class Hexite(CompositeReactionProduct):
    _name = 'Hexite'
    _item_id = ITEM_ID.HEXITE
    _unit_volume = 0.2


class Hyperflurite(CompositeReactionProduct):
    _name = 'Hyperflurite'
    _item_id = ITEM_ID.HYPERFLURITE
    _unit_volume = 0.2


class NeoMercurite(CompositeReactionProduct):
    _name = 'Neo Mercurite'
    _item_id = ITEM_ID.NEO_MERCURITE
    _unit_volume = 0.2


class PlatinumTechnite(CompositeReactionProduct):
    _name = 'Platinum Technite'
    _item_id = ITEM_ID.PLATINUM_TECHNITE
    _unit_volume = 0.2


class PromethiumMercurite(CompositeReactionProduct):
    _name = 'Promethium Mercurite'
    _item_id = ITEM_ID.PROMETHIUM_MERCURITE
    _unit_volume = 0.2


class Prometium(CompositeReactionProduct):
    _name = 'Prometium'
    _item_id = ITEM_ID.PROMETIUM
    _unit_volume = 0.2


class RolledTungstenAlloy(CompositeReactionProduct):
    _name = 'Rolled Tungsten Alloy'
    _item_id = ITEM_ID.ROLLED_TUNGSTEN_ALLOY
    _unit_volume = 0.2


class SiliconDiborite(CompositeReactionProduct):
    _name = 'Silicon Diborite'
    _item_id = ITEM_ID.SILICON_DIBORITE
    _unit_volume = 0.2


class Solerium(CompositeReactionProduct):
    _name = 'Solerium'
    _item_id = ITEM_ID.SOLERIUM
    _unit_volume = 0.2


class SulfuricAcid(CompositeReactionProduct):
    _name = 'Sulfuric Acid'
    _item_id = ITEM_ID.SULFURIC_ACID
    _unit_volume = 0.2


class ThuliumHafnite(CompositeReactionProduct):
    _name = 'ThuliumHafnite'
    _item_id = ITEM_ID.THULIUM_HAFNITE
    _unit_volume = 0.2


class TitaniumChromide(CompositeReactionProduct):
    _name = 'Titanium Chromide'
    _item_id = ITEM_ID.TITANIUM_CHROMIDE
    _unit_volume = 0.2



HYBRID_REACTION_PRODUCT_ITEMS = [CompositeReactionProduct]