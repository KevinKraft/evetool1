from src.constants.id import ITEM_ID

from src.object.recipe.reprocessable_item import ReprocessableItem

#-------------------------------------

class HybridReactionProduct(ReprocessableItem):
    pass


class Fulleroferrocene(HybridReactionProduct):
    _name = 'Fulleroferrocene'
    _item_id = ITEM_ID.FULLEROFERROCENE
    _unit_volume = 0.1


class PPDFullereneFibers(HybridReactionProduct):
    _name = 'PPD Fullerene Fibers'
    _item_id = ITEM_ID.PPD_FULLERENE_FIBERS
    _unit_volume = 0.5


class FullereneIntercalatedGraphite(HybridReactionProduct):
    _name = 'Fullerene Intercalated Graphite'
    _item_id = ITEM_ID.FULLERENE_INTERCALATED_GRAPHITE
    _unit_volume = 0.8


class Methanofullerene(HybridReactionProduct):
    _name = 'Methanofullerene'
    _item_id = ITEM_ID.METHANOFULLERENE
    _unit_volume = 0.75


class LanthanumMetallofullerene(HybridReactionProduct):
    _name = 'Lanthanum Metallofullerene'
    _item_id = ITEM_ID.LANTHANUM_METALLOFULLERENE
    _unit_volume = 1.


class ScandiumMetallofullerene(HybridReactionProduct):
    _name = 'Scandium Metallofullerene'
    _item_id = ITEM_ID.SCANDIUM_METALLOFULLERENE
    _unit_volume = 0.65


class GrapheneNanoribbons(HybridReactionProduct):
    _name = 'Graphene Nanoribons'
    _item_id = ITEM_ID.GRAPHENE_NANORIBBONS
    _unit_volume = 1.5


class Carbon86EpoxyResin(HybridReactionProduct):
    _name = 'Carbon-86 Epoxy Resin'
    _item_id = ITEM_ID.CARBON_86_EPOXY_RESIN
    _unit_volume = 0.4


class C3FTMAcid(HybridReactionProduct):
    _name = 'C3-FTM Acid'
    _item_id = ITEM_ID.C3_FTM_ACID
    _unit_volume = 0.2


HYBRID_REACTION_PRODUCT_ITEMS = [HybridReactionProduct, LanthanumMetallofullerene]