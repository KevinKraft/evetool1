#classes for information about gas items in eve and stacks of items and their values

#todo: Much of this can be populated from the API and using python schematics.

#-------------------------------------

from src.constants.id import ITEM_ID

from src.object.recipe.reprocessable_item import ReprocessableItem

#-------------------------------------

class HeliumFuelBlock(ReprocessableItem):
    _name = 'Helium Fuel Block'
    _item_id = ITEM_ID.HELIUM_FUEL_BLOCK
    _unit_volume = 5.


class NitrogenFuelBlock(ReprocessableItem):
    _name = 'Nitrogen Fuel Block'
    _item_id = ITEM_ID.NITROGEN_FUEL_BLOCK
    _unit_volume = 5.


class HydrogenFuelBlock(ReprocessableItem):
    _name = 'Hydrogen Fuel Block'
    _item_id = ITEM_ID.HYDROGEN_FUEL_BLOCK
    _unit_volume = 5.


class OxygenFuelBlock(ReprocessableItem):
    _name = 'Oxygen Fuel Block'
    _item_id = ITEM_ID.OXYGEN_FUEL_BLOCK
    _unit_volume = 5.


FUEL_ITEMS = [HeliumFuelBlock, NitrogenFuelBlock, HydrogenFuelBlock, OxygenFuelBlock]