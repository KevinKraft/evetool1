#classes for information about items in eve and stacks of items and their values

#todo: Much of this can be populated from the API and using python schematics.

#-------------------------------------

import logging
from math import floor
from typing import Union

from src.util.diagnostic import timeit

from src.metadata.read import get_name as database_read_name, get_volume as database_read_volume
from src.api.api import get_min_sell_price, get_max_buy_price, get_average_price
from src.util.isk_amount import ISKAmount

#-------------------------------------

class EveItem(object):

    _name = None
    _item_id = None
    _unit_volume = None

    @timeit
    def __init__(self, amount = 1, min_amount=1, name = None, item_id = None, unit_volume = None,
                 average_mode = False):

        if not name is None:
            self._name = name
        if not item_id is None:
            self._item_id = item_id
        if not unit_volume is None:
            self._unit_volume = unit_volume

        self._average_mode = average_mode

        if self._average_mode:
            self._unit_buy = None
            self._unit_sell = None
        else:
            self._unit_buy = self._make_price(get_min_sell_price(self._item_id, min_amount=min_amount))
            self._unit_sell = self._make_price(get_max_buy_price(self._item_id))

        if not amount is None:
            self.amount = amount

        if (self._unit_buy is None or self._unit_sell is None) and self._average_mode is False:
            logging.warning('No buy and/or sell prices supplied for item {}'.format(self._name))
            self._initialised = False
        else:
            self._initialised = True


    def __str__(self):
        return 'name: {}, amount {}, item_id {}'.format(self._name, self.amount, self._item_id)

    @property
    def amount(self):
        return self.___amount

    @amount.setter
    def amount(self, val):
        self.___amount = val

    @property
    def buy_price(self):
        return self.amount * self.unit_buy

    @property
    def item_id(self):
        return self._item_id

    @property
    def initialised(self):
        return self._initialised

    @property
    def name(self):
        return self._name

    @property
    def type_id(self):
        return self._item_id

    @property
    def unit_buy(self):
        if self._average_mode:
            logging.error(f'Item {self._name} is in average mode.')
        return self._unit_buy

    @property
    def unit_sell(self):
        if self._average_mode:
            logging.error(f'Item {self._name} is in average mode.')
        return self._unit_sell

    @property
    def unit_volume(self):
        return self._unit_volume

    def amount_in_given_volume(self, given_volume):
        return floor(given_volume / self._unit_volume)

    @timeit
    def average_buy_price(self, date=None):
        return self.average_price(date)

    @timeit
    def average_price(self, date=None):
        return self.amount * self.unit_average(date)

    @timeit
    def average_sell_price(self, full_sales_tax, date=None):
        value = self.average_price(date)
        return value - (value * full_sales_tax)

    def sales_tax(self, full_sales_tax):
        return self._base_sell_price() * full_sales_tax

    @timeit
    def sell_price(self, full_sales_tax):
        value = self._base_sell_price()
        return value - (value * full_sales_tax)

    def set_amount_to_fill(self, given_volume):
        self.amount = self.amount_in_given_volume(given_volume)

    @timeit
    def unit_average(self, date=None):
        if not self._average_mode:
            logging.error(f'Item {self._name} is not in average mode, but the average price is used.')
        return self._make_price(get_average_price(self._item_id, date))

    def volume(self):
        if self._unit_volume == None or self.amount == None or self.amount < 0:
            logging.warning('Item has no volume as the amount ({}) or unit volume are not set ({})'
                         .format(self.amount, self._unit_volume))
        return self._unit_volume * self.amount

    @timeit
    def _base_sell_price(self):
        return self.amount * self.unit_sell

    def _make_price(self, price):
        if price == None: return None
        return ISKAmount(price)

#-------------------------------------

def get_items_min_sell_prices(item_list, full_sales_tax):
    return [item.sell_price(full_sales_tax) for item in item_list]


def get_items_max_buy_prices(item_list):
    return [item.buy_price for item in item_list]

#-------------------------------------

@timeit
def get_item(type_id: int, amount: Union[int,float] = 1, min_amount: Union[int,float] = 1, average_mode: bool = False):
    name = database_read_name(type_id)
    vol = database_read_volume(type_id)
    return EveItem(amount, min_amount, name, type_id, vol, average_mode=average_mode)
