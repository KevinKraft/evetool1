#classes for information about misc items for manufacturing

#todo: Much of this can be populated from the API and using python schematics.

#-------------------------------------

from src.constants.id import ITEM_ID

from src.object.item.eve_item import EveItem

#-------------------------------------

class RAMWeaponTech(EveItem):
    _name = 'R.A.M.-Weapon Tech'
    _item_id = ITEM_ID.RAM_WEAPON_TECH
    _unit_volume = .04

class SuperconductorRails(EveItem):
    _name = 'Superconductor Rails'
    _item_id = ITEM_ID.SUPERCONDUCTOR_RAILS
    _unit_volume = 1.

MISC_MANUFACTURING_ITEMS = [RAMWeaponTech, SuperconductorRails]