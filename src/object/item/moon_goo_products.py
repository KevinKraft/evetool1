#classes for information about moon goo products in eve and stacks of items and their values

#todo: Much of this can be populated from the API and using python schematics.

#-------------------------------------

from src.constants.id import ITEM_ID

from .ore import OreItem

#-------------------------------------

class MoonGooProductItem(OreItem):
    pass


class Cadmium(MoonGooProductItem):
    _name = 'Cadmium'
    _item_id = ITEM_ID.CADMIUM
    _unit_volume = 0.05

class Caesium(MoonGooProductItem):
    _name = 'Caesium'
    _item_id = ITEM_ID.CAESIUM
    _unit_volume = 0.05


class Hydrocarbons(MoonGooProductItem):
    _name = 'Hydrocarbons'
    _item_id = ITEM_ID.HYDROCARBONS
    _unit_volume = 0.05


class Silicates(MoonGooProductItem):
    _name = 'Silicates'
    _item_id = ITEM_ID.SILICATES
    _unit_volume = 0.05


class EvaporiteDeposits(MoonGooProductItem):
    _name = 'Evaporite Deposits'
    _item_id = ITEM_ID.EVAPORITE_DEPOSITS
    _unit_volume = 0.05


class Cobalt(MoonGooProductItem):
    _name = 'Cobalt'
    _item_id = ITEM_ID.COBALT
    _unit_volume = 0.05


class Mercury(MoonGooProductItem):
    _name = 'Mercury'
    _item_id = ITEM_ID.MERCURY
    _unit_volume = 0.05


class Dysprosium(MoonGooProductItem):
    _name = 'Dysprosium'
    _item_id = ITEM_ID.DYSPROSIUM
    _unit_volume = 0.05


class Scandium(MoonGooProductItem):
    _name = 'Scandium'
    _item_id = ITEM_ID.SCANDIUM
    _unit_volume = 0.05


class Vanadium(MoonGooProductItem):
    _name = 'Vanadium'
    _item_id = ITEM_ID.VANADIUM
    _unit_volume = 0.05


class Hafnium(MoonGooProductItem):
    _name = 'Hafnium'
    _item_id = ITEM_ID.HAFNIUM
    _unit_volume = 0.05


class Neodymium(MoonGooProductItem):
    _name = 'Neodymium'
    _item_id = ITEM_ID.NEODYMIUM
    _unit_volume = 0.05


class Thulium(MoonGooProductItem):
    _name = 'Thulium'
    _item_id = ITEM_ID.THULIUM
    _unit_volume = 0.05


class Chromium(MoonGooProductItem):
    _name = 'Chromium'
    _item_id = ITEM_ID.CHROMIUM
    _unit_volume = 0.05


class Platinum(MoonGooProductItem):
    _name = 'Platinum'
    _item_id = ITEM_ID.PLATINUM
    _unit_volume = 0.05


class Promethium(MoonGooProductItem):
    _name = 'Promethium'
    _item_id = ITEM_ID.PROMETHIUM
    _unit_volume = 0.05


class Technetium(MoonGooProductItem):
    _name = 'Technetium'
    _item_id = ITEM_ID.TECHNETIUM
    _unit_volume = 0.05


class Tungsten(MoonGooProductItem):
    _name = 'Tungsten'
    _item_id = ITEM_ID.TUNGSTEN
    _unit_volume = 0.05


class AtmosphericGases(MoonGooProductItem):
    _name = 'Atmospheric Gases'
    _item_id = ITEM_ID.ATMOSPHERIC_GASES
    _unit_volume = 0.05


class Titanium(MoonGooProductItem):
    _name = 'Titanium'
    _item_id = ITEM_ID.TITANIUM
    _unit_volume = 0.05