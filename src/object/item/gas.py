#classes for information about gas items in eve and stacks of items and their values

#todo: Much of this can be populated from the API and using python schematics.

#-------------------------------------

from src.constants.id import ITEM_ID

from .ore import OreItem

#-------------------------------------

class GasItem(OreItem):
    pass


class Fullerite_C50(GasItem):
    _name = 'Fullerite-C50'
    _item_id = ITEM_ID.FULLERITE_C50
    _unit_volume = 1.


class Fullerite_C60(GasItem):
    _name = 'Fullerite-C60'
    _item_id = ITEM_ID.FULLERITE_C60
    _unit_volume = 1.


class Fullerite_C70(GasItem):
    _name = 'Fullerite-C70'
    _item_id = ITEM_ID.FULLERITE_C70
    _unit_volume = 1.


class Fullerite_C72(GasItem):
    _name = 'Fullerite-C72'
    _item_id = ITEM_ID.FULLERITE_C72
    _unit_volume = 2.


class Fullerite_C84(GasItem):
    _name = 'Fullerite-C84'
    _item_id = ITEM_ID.FULLERITE_C84
    _unit_volume = 2.


class Fullerite_C28(GasItem):
    _name = 'Fullerite-C28'
    _item_id = ITEM_ID.FULLERITE_C28
    _unit_volume = 2.


class Fullerite_C32(GasItem):
    _name = 'Fullerite-C32'
    _item_id = ITEM_ID.FULLERITE_C32
    _unit_volume = 5.


class Fullerite_C320(GasItem):
    _name = 'Fullerite-C320'
    _item_id = ITEM_ID.FULLERITE_C320
    _unit_volume = 5.


class Fullerite_C540(GasItem):
    _name = 'Fullerite-C540'
    _item_id = ITEM_ID.FULLERITE_C540
    _unit_volume = 10.


GAS_ITEMS = [
    Fullerite_C50, Fullerite_C60, Fullerite_C70, Fullerite_C72, Fullerite_C84, Fullerite_C28, Fullerite_C32,
    Fullerite_C320, Fullerite_C540
]