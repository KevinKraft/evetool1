import pytest
from mock import patch
from math import floor
from typing import List

from src.object.item.eve_item import EveItem, get_items_min_sell_prices, get_items_max_buy_prices, get_item
from src.util.isk_amount import ISKAmount


@pytest.fixture(scope='function')
def temp_item_list() -> List[EveItem]:
    with patch('src.object.item.eve_item.get_min_sell_price', return_value=1):
        with patch('src.object.item.eve_item.get_max_buy_price', return_value=2):
            yield [
                EveItem(10, 10, 'NAME1', 34, 5, False),
                EveItem(1, 2, 'NAME1', 50, 1, False),
                EveItem(200, 100, 'NAME1', 23, 56, False),
            ]


@pytest.mark.parametrize('amount, min_sell_order, max_buy_order, unit_volume',[
    (2, 2, 3, 2),
    (300, 0.1, 0.01, 9),
    (90, 60, 67, 40),
    (4000, 8000, 56, 6),
    (2, 1500000, 4000, 6000),
    (456, 2345, 3245, 5467),
    (23, 23, 3245, 4356456),
    (423, 3245, 8676867, 23),
    (435, 46, 234, 234),
    (2, 2, 2, 2),
])
def test_init(amount, min_sell_order, max_buy_order, unit_volume):

    with patch('src.object.item.eve_item.get_min_sell_price', return_value=min_sell_order) as i:
        with patch('src.object.item.eve_item.get_max_buy_price', return_value=max_buy_order) as j:
            with patch('src.object.item.eve_item.database_read_name', return_value='NAME') as k:
                with patch('src.object.item.eve_item.database_read_volume', return_value=unit_volume) as l:

                    for item in [EveItem(amount, amount, 'NAME', 34, unit_volume, False),
                             get_item(34, amount, amount, False)]:


                        assert item.name == 'NAME'
                        assert item.item_id == 34
                        assert item.unit_volume == unit_volume
                        assert item._average_mode is False
                        assert item.unit_buy == ISKAmount(min_sell_order)
                        assert item.unit_sell == ISKAmount(max_buy_order)
                        assert item.amount == amount
                        assert item.initialised is True
                        assert item.buy_price == ISKAmount(min_sell_order*amount)

                        full_sales_tax = 0.07
                        assert item.sell_price(full_sales_tax) == ISKAmount(max_buy_order * amount * (1 - full_sales_tax))
                        assert item.sales_tax(full_sales_tax) == ISKAmount(amount * max_buy_order * full_sales_tax)

                        given_volume = 34
                        assert item.amount_in_given_volume(given_volume) == floor(given_volume/unit_volume)


@pytest.mark.parametrize('amount, avg_price, unit_volume',[
    (2, 2, 2),
    (300, 0.1, 9),
    (90, 60, 40),
    (4000, 8000, 6),
    (2, 1500000, 6000),
    (456, 2345, 5467),
    (23, 23, 4356456),
    (423, 3245, 23),
    (435, 46, 234),
    (2, 2, 2),
])
def test_init_average(amount, avg_price, unit_volume):

    with patch('src.object.item.eve_item.get_average_price', return_value = avg_price) as i:
        with patch('src.object.item.eve_item.database_read_name', return_value='NAME') as k:
            with patch('src.object.item.eve_item.database_read_volume', return_value=unit_volume) as l:

                for item in [EveItem(amount, amount, 'NAME', 50, unit_volume, True),
                             get_item(50, amount, amount, True)]:

                    assert item.name == 'NAME'
                    assert item.item_id == 50
                    assert item.unit_volume == unit_volume
                    assert item._average_mode is True
                    assert item.amount == amount
                    assert item.initialised is True
                    assert item.unit_buy is None
                    assert item.unit_sell is None
                    assert item.average_buy_price() == ISKAmount(amount * avg_price)
                    assert item.average_price() == ISKAmount(avg_price * amount)

                    full_sales_tax = 0.06
                    assert item.average_sell_price(full_sales_tax) == ISKAmount(avg_price * amount * (1 - full_sales_tax))

                    assert item.unit_average() == ISKAmount(avg_price)

                    given_volume = 34
                    assert item.amount_in_given_volume(given_volume) == floor(given_volume/unit_volume)


def test_init_not_initialised():
    with patch('src.object.item.eve_item.get_min_sell_price', return_value=None):
        with patch('src.object.item.eve_item.get_max_buy_price', return_value=None):

            item = EveItem(10, 10, 'NAME', 50, 5, False)
            assert item.initialised is False


def test_set_amount_to_fill():
    with patch('src.object.item.eve_item.get_min_sell_price', return_value=1):
        with patch('src.object.item.eve_item.get_max_buy_price', return_value=1):

            item = EveItem(1, 1, 'NAME', 50, 5, False)
            assert item.initialised is True
            item.set_amount_to_fill(1000)
            assert item.amount == 200


def test_get_items_min_sell_prices(temp_item_list: List[EveItem]):
    full_sales_tax = 0.07
    actual = sorted(get_items_min_sell_prices(temp_item_list, full_sales_tax))
    expected = sorted([item.sell_price(full_sales_tax) for item in temp_item_list])
    assert actual == expected


def test_get_items_max_buy_prices(temp_item_list: List[EveItem]):
    actual = sorted(get_items_max_buy_prices(temp_item_list))
    expected = sorted([item.buy_price for item in temp_item_list])
    assert actual == expected
