#base class for all chaining recipes to get total ROIs

#-------------------------------------

from typing import Dict
import pickle

from src.metadata.read import get_recipe as get_blueprint_recipe, get_group
from src.model.metadata import BlueprintBaseActivity
from src.model.group_recipe_config import GroupRecipeConfig
from src.util.diagnostic import timeit
from src.object.recipe_chain.chain_config import CHAIN_CONFIG
from src.object.recipe.recipe import get_recipe, Recipe

#-------------------------------------

class RecipeChainInitException(Exception):
    pass

class RecipeChain(object):

    @timeit
    def __init__(self, type_id, runs=1.0, config_dict: Dict[int, GroupRecipeConfig] = CHAIN_CONFIG):
        self._type_id = type_id
        self._runs = runs
        self._config = config_dict
        self._bp = get_blueprint_recipe(self._type_id)
        self._group_id = get_group(self._type_id)

        if not self._requires_chain(self._bp):
            raise RecipeChainInitException(f'Given type ID {type_id} does not require a chain recipe.')

        if not self._requires_recipe(self._type_id):
            raise RecipeChainInitException(f'Given type ID {type_id} does not require a recipe.')

        self._system_cost_modifier = self._config[self._group_id].system_cost_modifier

        self._children = self._get_children()

        self._recipe = self._make_recipe(self._type_id, self._runs)

    @property
    def name(self):
        return self._recipe.name + 'Chain'

    @property
    def output_amount(self):
        return self._recipe.output_amount

    @timeit
    def cost_price(self, full_sales_tax, facility_tax_rate):
        full_cost = self.total_input_value(full_sales_tax, facility_tax_rate)
        return full_cost / (self.output_amount + 0.0)

    @timeit
    def markup(self, full_sales_tax, facility_tax_rate):
        output_value = self.output_value(full_sales_tax, facility_tax_rate)
        full_cost = self.total_input_value(full_sales_tax, facility_tax_rate)
        return (output_value / full_cost) * 100

    @timeit
    def profit(self, full_sales_tax, facility_tax_rate):
        return self.output_value(full_sales_tax, facility_tax_rate) - \
               self.total_input_value(full_sales_tax, facility_tax_rate)

    @timeit
    def profit_margin(self, full_sales_tax, facility_tax_rate):
        profit = self.profit(full_sales_tax, facility_tax_rate)
        full_input = self.total_input_value(full_sales_tax, facility_tax_rate)
        return (profit / full_input) * 100

    @timeit
    def profit_percentage(self, full_sales_tax, facility_tax_rate):
        profit = self.profit(full_sales_tax, facility_tax_rate)
        output_value = self.output_value(full_sales_tax, facility_tax_rate)
        return (profit / output_value) * 100

    @timeit
    def output_value(self, full_sales_tax, facility_tax_rate):
        _, _, output_value, _ = self.results(full_sales_tax, facility_tax_rate)
        return output_value

    def pickle_config(self, file: str):
        new_config = {}
        for key, value in self._config.items():
            new_config[key] = value.to_primitive()
        pickle.dump(new_config, open(file, 'wb'))

    @timeit
    def results(self, full_sales_tax, facility_tax_rate):
        '''Returns input_value, taxes_and_charges, output_value, profit, of this recipe.'''
        return self._recipe._profit(full_sales_tax, self._system_cost_modifier, facility_tax_rate)

    @timeit
    def total_input_value(self, full_sales_tax, facility_tax_rate):
        _, taxes_and_charges, _, _ = self.results(full_sales_tax, facility_tax_rate)
        total_input_value = taxes_and_charges
        for child in self._children:
            if isinstance(child, RecipeChain):
                child_value = child.total_input_value(full_sales_tax, facility_tax_rate)
            elif isinstance(child, Recipe):
                child_value = child.total_input_value(full_sales_tax, self._system_cost_modifier, facility_tax_rate)
            elif isinstance(child, int):
                child_value = self._recipe.input_item_value(child, full_sales_tax)
            else:
                raise ValueError(f'Unknown child type for recipe chain of type id {self._type_id}.')
            total_input_value += child_value
        return total_input_value

    def _get_children(self):
        children = []
        for mat in self._bp.materials:
            child_amount_per_run = mat.quantity
            child_id = mat.typeID
            child_bp = get_blueprint_recipe(child_id)

            if child_bp is not None:
                requires_chain = self._requires_chain(child_bp)
                requires_recipe = self._requires_recipe(child_id)
                if requires_chain or requires_recipe:
                    base_child_runs = self._get_runs_needed_for_child_amount(child_bp, child_amount_per_run)
                    child_runs = self._runs * base_child_runs
                    if requires_chain:
                        children.append(RecipeChain(mat.typeID, runs=child_runs, config_dict=self._config))
                    else:
                        children.append(self._make_recipe(child_id, child_runs))
            else:
                children.append(child_id)

        return children

    def _make_recipe(self, type_id: int, runs: float):
        group_id = get_group(type_id)
        config_entry = self._config[group_id]
        if config_entry.price_mode == 'buy':
            raise AttributeError(f"Chain config price mode for group id {group_id} is set to 'buy'. I can't think "
                                 f"of any reason why this is required.")

        recipe = get_recipe(runs, type_id, **config_entry.to_recipe_pars())
        if recipe is None:
            raise RecipeChainInitException(f'Failed to create recipe for type {type_id} in recipe chain for type '
                                           f'{self._type_id}.')
        return  recipe

    def _requires_chain(self, child_bp: BlueprintBaseActivity):
        #a chain is required if any one of the input items to the child require a chain.
        for mat in child_bp.materials:
            if self._requires_recipe(mat.typeID) is True:
                return True
        return False

    def _requires_recipe(self, type_id: int):
        '''a recipe is required if the item has an entry in the config, and thus we want to make it.'''
        group_id = get_group(type_id)
        if group_id in self._config.keys():
            return True
        return False

    @staticmethod
    def _get_runs_needed_for_child_amount(child_bp: BlueprintBaseActivity, base_amount: int):
        amount_per_run = child_bp.products[0].quantity
        return base_amount / (amount_per_run + 0.0)


