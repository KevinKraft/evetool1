#config for how parent and child recipes in a chain should be calculated

#-------------------------------------

from src.constants.id import GROUP_ID
from src.constants.fuel_items import FUEL_GROUPS
from src.model.group_recipe_config import GroupRecipeConfig

#-------------------------------------

CHAIN_CONFIG = {
    GROUP_ID.COMPOSITE : GroupRecipeConfig(dict(
        price_mode='sell',
        fuel_item_groups=FUEL_GROUPS.COMPOSITES,
        system_cost_modifier=0.03)),
    GROUP_ID.INTERMEDIATE_MATERIAL: GroupRecipeConfig(dict(
        price_mode='sell',
        fuel_item_groups=FUEL_GROUPS.INTERMEDIATE_MATERIAL,
        system_cost_modifier=0.03)),
    GROUP_ID.HYBRID_POLYMER: GroupRecipeConfig(dict(
        price_mode='sell',
        fuel_item_groups=FUEL_GROUPS.HYBRID_POLYMER,
        system_cost_modifier=0.03)),
    GROUP_ID.HYBRID_TECH_COMPONENT: GroupRecipeConfig(dict(
        price_mode='sell',
        fuel_item_groups=FUEL_GROUPS.HYBRID_TECH_COMPONENT,
        system_cost_modifier=0.03)),
    GROUP_ID.CONSTRUCTION_COMPONENT: GroupRecipeConfig(dict(
        price_mode='sell',
        system_cost_modifier=0.03)),
    GROUP_ID.HEAVY_ASSAULT_CRUISER: GroupRecipeConfig(dict(
        price_mode='sell',
        fuel_item_groups=FUEL_GROUPS.HEAVY_ASSAULT_CRUISER,
        system_cost_modifier=0.03)),
    GROUP_ID.CRUISER: GroupRecipeConfig(dict(
        price_mode='sell',
        fuel_item_groups=FUEL_GROUPS.CRUISER,
        system_cost_modifier=0.03)),
}