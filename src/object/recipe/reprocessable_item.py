from src.object.item.eve_item import EveItem

#-------------------------------------

class ReprocessableItem(EveItem):
    _name = 'ReprocessableItem'
    _reprocessed_materials = None
    _reprocess_load = None

REPROCESSABLE_ITEMS = [ReprocessableItem]