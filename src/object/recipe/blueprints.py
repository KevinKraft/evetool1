#recepie classes blueprints

#-------------------------------------

import logging
from datetime import timedelta

from src.object.recipe.recipe import Recipe
from src.object.item.mineral import Pyerite, Mexallon, Isogen, Tritanium, Morphite
from src.object.item.fittings import N125MMRailgunI, N125MMRailgunII
from src.object.item.misc_manufacturing import RAMWeaponTech, SuperconductorRails
from src.object.item.pi_products import Robotics

#-------------------------------------

class Blueprint(Recipe):

    _time_per_run = None

    @property
    def total_time(self):
        return self._time_per_run * self.runs

    def job_summary(self, full_sales_tax, system_cost_index, facility_tax_rate):
        super(Blueprint, self).job_summary(full_sales_tax, system_cost_index, facility_tax_rate)
        logging.info('Total Time: '+str(self.total_time))

#-------------------------------------

class N125MMRailgunIBlueprint(Blueprint):

    _input_items = [
        Tritanium(1311, min_amount=1311),
        Pyerite(473, min_amount=473),
        Mexallon(103, min_amount=103),
        Isogen(2, min_amount=2),
    ]

    _output_items = [
        N125MMRailgunI(1)
    ]

    _time_per_run = timedelta(minutes=15)

class N125MMRailgunIIBlueprint(Blueprint):

    _input_items = [
        N125MMRailgunI(1, min_amount=1),
        RAMWeaponTech(1, min_amount=1),
        SuperconductorRails(8, min_amount=8),
        Morphite(5, min_amount=5),
        Robotics(1, min_amount=1),
    ]

    _output_items = [
        N125MMRailgunII(1)
    ]

    _time_per_run = timedelta(minutes=39)