#recepie classes for composite reactions

#-------------------------------------

from datetime import timedelta

from src.object.recipe.reaction import Reaction
from src.object.item.moon_goo_products import Cadmium, Caesium, Hydrocarbons, Silicates, EvaporiteDeposits, Cobalt, \
    Mercury, Dysprosium, Scandium, Vanadium, Hafnium, Neodymium, Thulium, Chromium, Platinum, Promethium, Technetium, \
    Tungsten, AtmosphericGases, Titanium
from src.object.item.fuel import OxygenFuelBlock, HydrogenFuelBlock, NitrogenFuelBlock, HeliumFuelBlock
from src.object.item.composite_reaction_products import CaesariumCadmide, CarbonPolymers, CeramicPowder, \
    CrystalliteAlloy, Dysporite, FerniteAlloy, Ferrofluid, FluxedCondensates, Hexite, Hyperflurite, NeoMercurite, \
    PlatinumTechnite, PromethiumMercurite, Prometium, RolledTungstenAlloy, SiliconDiborite, Solerium, SulfuricAcid, \
    ThuliumHafnite, TitaniumChromide


#-------------------------------------

class CompositeReaction(Reaction):
    pass

#-------------------------------------

class CaesariumCadmideReactionFormula(CompositeReaction):
    _fuel_items = [
        OxygenFuelBlock(5, min_amount=5),
    ]
    _input_items = [
        Cadmium(100, min_amount=100),
        Caesium(100, min_amount=100),
    ]

    _output_items = [
        CaesariumCadmide(200)
    ]

    _time_per_run = timedelta(hours=3)


class CarbonPolymersReactionFormula(CompositeReaction):
    _fuel_items = [
        HeliumFuelBlock(5, min_amount=5),
    ]
    _input_items = [
        Hydrocarbons(100, min_amount=100),
        Silicates(100, min_amount=100),
    ]

    _output_items = [
        CarbonPolymers(200)
    ]

    _time_per_run = timedelta(hours=3)


class CeramicPowderReactionFormula(CompositeReaction):
    _fuel_items = [
        HydrogenFuelBlock(5, min_amount=5),
    ]
    _input_items = [
        EvaporiteDeposits(100, min_amount=100),
        Silicates(100, min_amount=100),
    ]

    _output_items = [
        CeramicPowder(200)
    ]

    _time_per_run = timedelta(hours=3)


class CrystalliteAlloyReactionFormula(CompositeReaction):
    _fuel_items = [
        HeliumFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Cobalt(100, min_amount=100),
        Cadmium(100, min_amount=100),
    ]

    _output_items = [
        CrystalliteAlloy(200)
    ]

    _time_per_run = timedelta(hours=3)


class DysporiteReactionFormula(CompositeReaction):
    _fuel_items = [
        HeliumFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Mercury(100, min_amount=100),
        Dysprosium(100, min_amount=100),
    ]

    _output_items = [
        Dysporite(200)
    ]

    _time_per_run = timedelta(hours=3)


class FerniteAlloyReactionFormula(CompositeReaction):
    _fuel_items = [
        HydrogenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Scandium(100, min_amount=100),
        Vanadium(100, min_amount=100),
    ]

    _output_items = [
        FerniteAlloy(200)
    ]

    _time_per_run = timedelta(hours=3)


class FerrofluidReactionFormula(CompositeReaction):
    _fuel_items = [
        HydrogenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Hafnium(100, min_amount=100),
        Dysprosium(100, min_amount=100),
    ]

    _output_items = [
        Ferrofluid(200)
    ]

    _time_per_run = timedelta(hours=3)


class FluxedCondensatesReactionFormula(CompositeReaction):
    _fuel_items = [
        OxygenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Neodymium(100, min_amount=100),
        Thulium(100, min_amount=100),
    ]

    _output_items = [
        FluxedCondensates(200)
    ]

    _time_per_run = timedelta(hours=3)


class HexiteReactionFormula(CompositeReaction):
    _fuel_items = [
        NitrogenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Chromium(100, min_amount=100),
        Platinum(100, min_amount=100),
    ]

    _output_items = [
        Hexite(200)
    ]

    _time_per_run = timedelta(hours=3)


class HyperfluriteReactionFormula(CompositeReaction):
    _fuel_items = [
        NitrogenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Vanadium(100, min_amount=100),
        Promethium(100, min_amount=100),
    ]

    _output_items = [
        Hyperflurite(200)
    ]

    _time_per_run = timedelta(hours=3)


class NeoMercuriteReactionFormula(CompositeReaction):
    _fuel_items = [
        HeliumFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Mercury(100, min_amount=100),
        Neodymium(100, min_amount=100),
    ]

    _output_items = [
        NeoMercurite(200)
    ]

    _time_per_run = timedelta(hours=3)


class PlatinumTechniteReactionFormula(CompositeReaction):
    _fuel_items = [
        NitrogenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Platinum(100, min_amount=100),
        Technetium(100, min_amount=100),
    ]

    _output_items = [
        PlatinumTechnite(200)
    ]

    _time_per_run = timedelta(hours=3)


class PromethiumMercuriteReactionFormula(CompositeReaction):
    _fuel_items = [
        HeliumFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Mercury(100, min_amount=100),
        Promethium(100, min_amount=100),
    ]

    _output_items = [
        PromethiumMercurite(200)
    ]

    _time_per_run = timedelta(hours=3)


class PrometiumReactionFormula(CompositeReaction):
    _fuel_items = [
        OxygenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Cadmium(100, min_amount=100),
        Promethium(100, min_amount=100),
    ]

    _output_items = [
        Prometium(200)
    ]

    _time_per_run = timedelta(hours=3)


class RolledTungstenAlloyReactionFormula(CompositeReaction):
    _fuel_items = [
        NitrogenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Tungsten(100, min_amount=100),
        Platinum(100, min_amount=100),
    ]

    _output_items = [
        RolledTungstenAlloy(200)
    ]

    _time_per_run = timedelta(hours=3)


class SiliconDiboriteReactionFormula(CompositeReaction):
    _fuel_items = [
        OxygenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        EvaporiteDeposits(100, min_amount=100),
        Silicates(100, min_amount=100),
    ]

    _output_items = [
        SiliconDiborite(200)
    ]

    _time_per_run = timedelta(hours=3)


class SoleriumReactionFormula(CompositeReaction):
    _fuel_items = [
        OxygenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Chromium(100, min_amount=100),
        Caesium(100, min_amount=100),
    ]

    _output_items = [
        Solerium(200)
    ]

    _time_per_run = timedelta(hours=3)


class SulfuricAcidReactionFormula(CompositeReaction):
    _fuel_items = [
        NitrogenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        AtmosphericGases(100, min_amount=100),
        EvaporiteDeposits(100, min_amount=100),
    ]

    _output_items = [
        SulfuricAcid(200)
    ]

    _time_per_run = timedelta(hours=3)


class ThuliumHafniteReactionFormula(CompositeReaction):
    _fuel_items = [
        OxygenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Hafnium(100, min_amount=100),
        Thulium(100, min_amount=100),
    ]

    _output_items = [
        ThuliumHafnite(200)
    ]

    _time_per_run = timedelta(hours=3)


class TitaniumChromideReactionFormula(CompositeReaction):
    _fuel_items = [
        OxygenFuelBlock(5, min_amount=5),
    ]

    _input_items = [
        Titanium(100, min_amount=100),
        Chromium(100, min_amount=100),
    ]

    _output_items = [
        TitaniumChromide(200)
    ]

    _time_per_run = timedelta(hours=3)


ALL_COMPOSITE_REACTIONS = [CaesariumCadmideReactionFormula, CarbonPolymersReactionFormula, CeramicPowderReactionFormula,
                           CrystalliteAlloyReactionFormula, DysporiteReactionFormula, FerniteAlloyReactionFormula,
                           FerrofluidReactionFormula, FluxedCondensatesReactionFormula, HexiteReactionFormula,
                           HyperfluriteReactionFormula, NeoMercuriteReactionFormula, PlatinumTechniteReactionFormula,
                           PromethiumMercuriteReactionFormula, PrometiumReactionFormula,
                           RolledTungstenAlloyReactionFormula, SiliconDiboriteReactionFormula, SoleriumReactionFormula,
                           SulfuricAcidReactionFormula, ThuliumHafniteReactionFormula, TitaniumChromideReactionFormula]
