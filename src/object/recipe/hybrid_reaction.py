#recepie classes for hybrid reactions

#-------------------------------------

from datetime import timedelta

from src.object.recipe.reaction import Reaction
from src.object.item.gas import Fullerite_C50, Fullerite_C60, Fullerite_C84, Fullerite_C70, Fullerite_C540, Fullerite_C32, \
    Fullerite_C320, Fullerite_C72, Fullerite_C28
from src.object.item.fuel import OxygenFuelBlock, HydrogenFuelBlock, NitrogenFuelBlock, HeliumFuelBlock
from src.object.item.mineral import Nocxium, Pyerite, Megacyte, Mexallon, Isogen, Zydrine, Tritanium
from src.object.item.hybrid_reaction_products import LanthanumMetallofullerene, PPDFullereneFibers, FullereneIntercalatedGraphite, \
    C3FTMAcid, GrapheneNanoribbons, Carbon86EpoxyResin, Methanofullerene, ScandiumMetallofullerene, Fulleroferrocene


#-------------------------------------

class HybridReaction(Reaction):
    pass

#-------------------------------------

class C3FTMAcidReaction(HybridReaction):

    _input_items = [
        HeliumFuelBlock(5, min_amount=5),
        Megacyte(80, min_amount=80),
        Fullerite_C84(100, min_amount=100),
        Fullerite_C540(100, min_amount=100)
    ]

    _output_items = [
        C3FTMAcid(100)
    ]

    _time_per_run = timedelta(hours=3)


class Carbon86EpoxyResinReaction(HybridReaction):

    _input_items = [
        NitrogenFuelBlock(5, min_amount=5),
        Zydrine(30, min_amount=30),
        Fullerite_C32(100, min_amount=100),
        Fullerite_C320(100, min_amount=100)
    ]

    _output_items = [
        Carbon86EpoxyResin(160)
    ]

    _time_per_run = timedelta(hours=3)



class FullereneIntercalatedGraphiteReaction(HybridReaction):

    _input_items = [
        HydrogenFuelBlock(5, min_amount=5),
        Mexallon(600, min_amount=600),
        Fullerite_C60(100, min_amount=100),
        Fullerite_C70(100, min_amount=100)
    ]

    _output_items = [
        FullereneIntercalatedGraphite(120)
    ]

    _time_per_run = timedelta(hours=3)


class FulleroferroceneReaction(HybridReaction):

    _input_items = [
        OxygenFuelBlock(5, min_amount=5),
        Tritanium(1000, min_amount=1000),
        Fullerite_C60(100, min_amount=100),
        Fullerite_C50(200, min_amount=200)
    ]

    _output_items = [
        Fulleroferrocene(1000)
    ]

    _time_per_run = timedelta(hours=3)


class GrapheneNanoribbonsReaction(HybridReaction):

    _input_items = [
        NitrogenFuelBlock(5, min_amount=5),
        Nocxium(400, min_amount=400),
        Fullerite_C28(100, min_amount=100),
        Fullerite_C32(100, min_amount=100)
    ]

    _output_items = [
        GrapheneNanoribbons(120)
    ]

    _time_per_run = timedelta(hours=3)


class LanthanumMetallofullereneReaction(HybridReaction):

    _input_items = [
        Nocxium(200, min_amount=200),
        Fullerite_C70(100, min_amount=100),
        Fullerite_C84(100, min_amount=100),
        OxygenFuelBlock(5, min_amount=5)
    ]

    _output_items = [
        LanthanumMetallofullerene(120)
    ]

    _time_per_run = timedelta(hours=3)


class MethanofullereneReaction(HybridReaction):

    _input_items = [
        Isogen(300, min_amount=300),
        Fullerite_C70(100, min_amount=100),
        Fullerite_C72(100, min_amount=100),
        HydrogenFuelBlock(5, min_amount=5)
    ]

    _output_items = [
        Methanofullerene(160)
    ]

    _time_per_run = timedelta(hours=3)


class PPDFullereneFibersReaction(HybridReaction):

    _input_items = [
        Pyerite(800, min_amount=800),
        Fullerite_C60(100, min_amount=100),
        Fullerite_C50(300, min_amount=300),
        HydrogenFuelBlock(5, min_amount=5)
    ]

    _output_items = [
        PPDFullereneFibers(250)
    ]

    _time_per_run = timedelta(hours=3)


class ScandiumMetallofullereneReaction(HybridReaction):

    _input_items = [
        Zydrine(25, min_amount=25),
        Fullerite_C72(100, min_amount=100),
        Fullerite_C28(100, min_amount=100),
        HeliumFuelBlock(5, min_amount=5)
    ]

    _output_items = [
        ScandiumMetallofullerene(160)
    ]

    _time_per_run = timedelta(hours=3)


ALL_HYBRID_REACTIONS = [C3FTMAcidReaction, Carbon86EpoxyResinReaction, FullereneIntercalatedGraphiteReaction,
                        FulleroferroceneReaction, GrapheneNanoribbonsReaction, LanthanumMetallofullereneReaction,
                        MethanofullereneReaction, PPDFullereneFibersReaction, ScandiumMetallofullereneReaction]