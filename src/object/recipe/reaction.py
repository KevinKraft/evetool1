#recepie classes for reactions

#-------------------------------------

import logging

from src.object.recipe.recipe import Recipe

#-------------------------------------

class Reaction(Recipe):

    _time_per_run = None

    @property
    def total_time(self):
        return self._time_per_run * self.runs

    def job_summary(self, full_sales_tax, system_cost_index, facility_tax_rate):
        super(Reaction, self).job_summary(full_sales_tax, system_cost_index, facility_tax_rate)
        logging.info('Total Time: '+str(self.total_time))
