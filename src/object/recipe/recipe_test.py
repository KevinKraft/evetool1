import pytest
from mock import patch, MagicMock

from src.object.recipe.recipe import Recipe
from src.object.item.eve_item import EveItem


#MOCK THE API SO THAT THE VALUES RETURNS PER ITEM TYPE ARE ALWAYS THE SAME

# class ItemMock(object):
#     def __init__(self, amount, unit_buy, unit_sell, name=None, type_id=None, unit_volume=None):
#         self.amount = amount
#
#
# def mock_get_item(type_id: int, amount: int = 1, min_amount: int = 1, average_mode: bool = False):
#     return ItemMock(amount, )


# #INIT TESTS, WITH CONSTRUCTOR AND GET RECIPE NORMAL MODE, BUY
# @pytest.mark.parametrize('runs, input_items, output_item, fuel_items',[
#     (1, [ItemMock(1, 'tn1', 1, 1)], ItemMock(1, 'tn2', 2, 1), []),
# ])
# def test_init_normal_buy_mode(runs, input_items, output_item, fuel_items):
#
#     with patch('src.object.recipe.recipe.get_item', new=mock_get_item):
#
#         for recipe in [Recipe(runs=1, input_items=input_items, output_items=[output_item], fuel_items=fuel_items)]:
#
#             assert recipe._input_items == input_items
#             assert recipe._output_items == [output_item]
#             assert recipe._fuel_items == fuel_items
#             assert recipe.runs == runs
#             assert recipe._price_mode == 'buy'
#             assert recipe._average_mode is False
#             assert recipe.name == 'tn2Recipe'
#             for item, base_item in zip(recipe.input_items, input_items):
#                 assert item.amount == base_item.amount * runs
#             actual_outputs = recipe.output_items
#             assert len(actual_outputs) == 1
#             for item, base_item in zip(recipe.output_items, [output_item]):
#                 assert item.amount == base_item.amount * runs
#
#             full_sales_tax = 0.12
#             for item in input_items:
#                 assert recipe._get_input_value(item, full_sales_tax) == item.buy_price
#
#             #_input_items_base_price


#INIT TESTS, WITH CONSTRUCTOR AND GET RECIPE, NORMAL MODE SELL

#INIT TESTS, WITH CONSTRUCTOR AND GET RECIPE, AVERAGE MODE BUY

#INIT TESTS, WITH CONSTRUCTOR AND GET RECIPE, AVERAGE MODE SELL

#INIT TESTS, WITH CONSTRUCTOR AND GET RECIPE, WITH DATE

#TEST FOR PROFIT MARGIN

#TEST FOR PROFOT PERCENTAGE

#TEST FOR MARKUP

#TEST FOR COST PRICE

#CROSS CHECK OF METRICS TEST, MAKE SURE THEY ALL MAKE SENSE IN RELSTION TO EACH OTHER.

#REACTION OR MANUFACTURE TESTS

#RECIPE FROM BLUEPRINT TEST
