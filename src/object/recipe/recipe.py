#base class for all reprocessing, manufacturing, reaction, etc. processes.

#-------------------------------------

import sys
import logging
from datetime import timedelta
from math import ceil

from src.util import standard_name
from src.util.diagnostic import timeit

from src.metadata.read import get_recipe as read_database_recipe, get_all_types as read_database_all_types, \
    is_reaction, get_recipe_from_bp as read_database_recipe_from_bp, is_blueprint_reaction
from src.model.metadata import BlueprintBaseActivity
from src.object.item.eve_item import get_item

#-------------------------------------

class Recipe(object):

    _input_items = []
    _output_items = []

    #included as input items, but the buy price will always be used to calcualte their value, even if _buy_inputs
    #is False. This is to allow, for example, to compare the profits for either selling moon goo, or running
    #reactions and selling the products. In such a calculation for moon goo sell price is used, but the fuel block
    #buy price should be used.
    _fuel_items = []

    @timeit
    def __init__(self, runs=1.0, price_mode='buy', average_mode=False, date=None,
                 input_items = None, output_items = None, fuel_items = None, mat_eff: int = 0,
                 mat_eff_round: bool = False):
        """
        :param runs: Number of runs of the recipe. Can be float.
        :param price_mode: If 'buy' the input cost is the cost to buy the items, if 'sell' the input cost is what you \
        would get if the items were sold directly. 'buy': use the buy price, 'sell': use the sell price. There are \
        certain fuel items defined for a recipe where the buy price is used regardless of the price mode.
        :param average_mode: If True then the most recent daily average price is used, or optionally a historic date \
        can be given. The sales taxes are still applied to the output items in this case.
        :param date: Datetime object for when the average option is used. Defaults to the most recent available date.
        :param mat_eff: The percentage number (0-10) of the material efficiency of the blueprint.
        :param mat_eff_round: If True the actual way the game does material efficiency is used, it rounds the number \
        of items needed up to the nearest whole. If False the float amount is used.
        """

        if not input_items is None:
            self._input_items = input_items
        if not output_items is None:
            self._output_items = output_items
        if not fuel_items is None:
            self._fuel_items = fuel_items

        self._runs = runs
        if price_mode not in ['buy', 'sell']:
            logging.error("Given price mode must be one of 'buy' or 'sell', got " + str(price_mode) + ".")
            sys.exit(1)
        self._price_mode = price_mode
        self._average_mode = average_mode
        self._date = date
        if self._date != None:
            if self.has_data_on_date() is False:
                logging.warning('One or more items for {0} has no average price data for date {1}'
                      .format(self.name, self._date))

        self._mat_eff = mat_eff
        self._mat_eff_round = mat_eff_round


    @property
    def name(self):
        return standard_name(self._output_items[0].name) + 'Recipe'

    @property
    def runs(self):
        return self._runs

    @property
    def input_items(self):
        items_for_n_runs = self._scale_item_amounts_by_runs(self._input_items + self._fuel_items)
        return self._apply_material_efficiency(items_for_n_runs)

    @property
    def output_amount(self):
        return self.output_items[0].amount

    @property
    def output_items(self):
        return self._scale_item_amounts_by_runs(self._output_items)

    @timeit
    def cost_price(self, full_sales_tax):
        item_cost = sum([self._get_input_value(item, full_sales_tax) for item in self.input_items])
        out_items = self.output_items
        if len(out_items) > 1:
            logging.warning('Cost price cannot be calculated if there are more than one output item types.')
            return None
        return item_cost / self.output_items[0].amount + 0.0

    @timeit
    def full_job_cost(self, system_cost_index, facility_tax_rate):
        job_cost = self._job_cost(system_cost_index)
        return job_cost + ( job_cost * facility_tax_rate)

    @timeit
    def has_data_on_date(self):
        for item in self.input_items + self.output_items:
            if item.unit_average(self._date) == None:
                return False
        return True

    def input_item_summary(self, full_sales_tax):
        self._item_summary(self.input_items, self._price_mode, full_sales_tax)

    def input_item_value(self, type_id: int, full_sales_tax: float):
        '''
        Returns the total value of the input item of the given type, account for the amount of that item used. Throws
        an error if this recipe doesn't have the item
        '''
        for item in self.input_items:
            if item.type_id == type_id:
                return self._get_input_value(item, full_sales_tax)
        raise AttributeError(f'Recipe {self.name} does not use item type id {type_id}.')


    def item_summary(self, full_sales_tax):
        logging.info('Inputs:')
        self.input_item_summary(full_sales_tax)
        logging.info('Outputs:')
        self.output_item_summary(full_sales_tax)

    def job_summary(self, full_sales_tax, system_cost_index, facility_tax_rate):
        logging.info('Runs: '+str(self.runs))
        logging.info('Job cost: '+str(self._job_cost(system_cost_index)))
        logging.info('Full job cost: '+str(self.full_job_cost(system_cost_index, facility_tax_rate)))
        self.item_summary(full_sales_tax)

    def output_item_summary(self, full_sales_tax):
        self._item_summary(self.output_items, 'sell', full_sales_tax)

    @timeit
    def markup(self, full_sales_tax, system_cost_index, facility_tax_rate):
        input_value, taxes_and_charges, output_value, profit = self._profit(full_sales_tax, system_cost_index, facility_tax_rate)
        full_cost = input_value + taxes_and_charges
        return (output_value / full_cost) * 100

    @timeit
    def profit(self, full_sales_tax, system_cost_index, facility_tax_rate):
        _, _, _, profit = self._profit(full_sales_tax, system_cost_index, facility_tax_rate)
        return profit

    @timeit
    def profit_margin(self, full_sales_tax, system_cost_index, facility_tax_rate):
        _, _, output_value, profit = self._profit(full_sales_tax, system_cost_index, facility_tax_rate)
        return (profit / output_value) * 100

    @timeit
    def profit_percentage(self, full_sales_tax, system_cost_index, facility_tax_rate):
        input_value, taxes_and_charges, _, profit = self._profit(full_sales_tax, system_cost_index, facility_tax_rate)
        full_cost = input_value + taxes_and_charges
        return (profit / full_cost) * 100

    def total_input_value(self, full_sales_tax, system_cost_index, facility_tax_rate):
        input_value, taxes_and_charges, _, _ = self._profit(full_sales_tax, system_cost_index, facility_tax_rate)
        return input_value + taxes_and_charges

    def total_output_value(self, full_sales_tax, system_cost_index, facility_tax_rate):
        _, _, output_value, _ = self._profit(full_sales_tax, system_cost_index, facility_tax_rate)
        return output_value

    def _apply_material_efficiency(self, item_list):
        items = []
        for item in item_list:
            tot_amount = item.amount * (1.0 - self._mat_eff / 100.0)
            if self._mat_eff_round:
                tot_amount = ceil(tot_amount)
            items.append(get_item(item.item_id, tot_amount, min_amount=tot_amount, average_mode=self._average_mode))
        return items

    @timeit
    def _profit(self, full_sales_tax, system_cost_index, facility_tax_rate):
        input_value = self._input_items_base_price(full_sales_tax)
        if self._average_mode:
            output_value = sum([ item.average_sell_price(full_sales_tax, self._date) for item in self.output_items ])
        else:
            output_value = sum([item.sell_price(full_sales_tax) for item in self.output_items])
        taxes_and_charges = self.full_job_cost(system_cost_index, facility_tax_rate)
        profit = output_value - input_value - taxes_and_charges
        return input_value, taxes_and_charges, output_value, profit

    @timeit
    def _input_items_base_price(self, full_sales_tax):
        prices = [self._get_input_value(item, full_sales_tax) for item in self.input_items]
        return sum(prices)

    @timeit
    def _input_items_base_buy_price(self):
        if self._average_mode:
            return sum([item.average_buy_price(date=self._date) for item in self.input_items])
        else:
            return sum([item.buy_price for item in self.input_items])

    def _item_summary(self, item_list, price_mode = 'buy', full_sales_tax=0):
        display = 'name    |   price   |  amount  | unit price  | buy/sell |\n'
        price_list = []
        unit_price_list = []
        buy_or_sell = []
        for item in item_list:

            is_fuel_item = self._is_fuel_item(item)

            if price_mode == 'buy' or is_fuel_item:
                if self._average_mode:
                    price_list.append(item.average_buy_price(self._date))
                    unit_price_list.append(item.unit_average(self._date))
                else:
                    price_list.append(item.buy_price)
                    unit_price_list.append(item.unit_buy)
                buy_or_sell.append('buy')
            elif price_mode == 'sell':
                if self._average_mode:
                    price_list.append(item.average_sell_price(full_sales_tax, self._date))
                    unit_price_list.append(item.unit_average)
                else:
                    price_list.append(item.sell_price(full_sales_tax))
                    unit_price_list.append(item.unit_sell)
                buy_or_sell.append('sell')

        for i, item in enumerate(item_list):
            istr = '{} | {} | {} | {} | {} |\n'
            display += istr.format(item.name, price_list[i], item.amount, unit_price_list[i], buy_or_sell[i])
        logging.info(display+'Total: ' + str(sum(price_list)) + '\n')

    @timeit
    def _job_cost(self, system_cost_index):
        #https://www.eveonline.com/article/industry-3rd-party-developers
        #todo: In game the base cost is taken form averages available from the API, not the actual order prices.
        return self._input_items_base_buy_price() * system_cost_index

    @timeit
    def _scale_item_amounts_by_runs(self, item_list):
        items = []
        for item in item_list:
            tot_amount = (item.amount + 0.0)*self.runs
            items.append(get_item(item.item_id, tot_amount, min_amount=tot_amount, average_mode=self._average_mode))
        return items

    @timeit
    def _get_input_value(self, item, full_sales_tax):

        if self._price_mode == 'buy' or self._is_fuel_item(item):
            if self._average_mode:
                return item.average_buy_price(self._date)
            else:
                return item.buy_price
        elif self._price_mode == 'sell':
            if self._average_mode:
                return item.average_sell_price(full_sales_tax, self._date)
            else:
                return item.sell_price(full_sales_tax)

    def _is_fuel_item(self, item):
        return item.item_id in [f.item_id for f in self._fuel_items]

#-------------------------------------

class ReactionOrManufacture(Recipe):

    _time_per_run: timedelta = None

    def __init__(self, runs=1.0, price_mode='buy', average_mode=False, date=None,
                 input_items = None, output_items = None, fuel_items = None,
                 time_per_run: timedelta = None, mat_eff: float = 0, mat_eff_round: bool = False):
        super(ReactionOrManufacture, self).__init__(runs=runs, price_mode=price_mode, average_mode=average_mode,
                                                    date=date, input_items=input_items, output_items=output_items,
                                                    fuel_items=fuel_items, mat_eff=mat_eff, mat_eff_round=mat_eff_round)

        if not time_per_run is None:
            self._time_per_run = time_per_run

    @property
    def total_time(self):
        return self._time_per_run * self.runs

    def job_summary(self, full_sales_tax, system_cost_index, facility_tax_rate):
        super(ReactionOrManufacture, self).job_summary(full_sales_tax, system_cost_index, facility_tax_rate)
        logging.info('Total Time: '+str(self.total_time))


class Manufacture(ReactionOrManufacture):

    @property
    def name(self):
        return standard_name(self._output_items[0].name) + 'Manufacture'


class Reaction(ReactionOrManufacture):

    @property
    def name(self):
        return standard_name(self._output_items[0].name) + 'Reaction'

#-------------------------------------

@timeit
def _get_recipe(runs: float, recipe_model, is_reaction: bool, price_mode='buy', average_mode=False, date=None,
                fuel_item_groups = None, mat_eff: float = 0, mat_eff_round: bool = False):

    inputs = _inputs_from_model(recipe_model, average_mode=average_mode)
    outputs = _products_from_model(recipe_model, average_mode=average_mode)

    if inputs is None or outputs is None:
        logging.warning(f'Failed to initialise recipe for model {recipe_model.to_primitive()}.')
        return None

    fuel_items_list = []
    if not fuel_item_groups is None:
        for group in fuel_item_groups:
            fuel_items_list += read_database_all_types(group)

    fuel_inputs = []
    final_inputs = []
    for input in inputs:
        if input.item_id in fuel_items_list:
            fuel_inputs.append(input)
        else:
            final_inputs.append(input)

    run_time_s = recipe_model['time']

    if is_reaction:
        constr = Reaction
    else:
        constr = Manufacture

    return constr(runs, price_mode=price_mode, average_mode=average_mode, date=date,
                  input_items=final_inputs, output_items=outputs, fuel_items=fuel_inputs,
                  time_per_run=timedelta(seconds=run_time_s), mat_eff=mat_eff, mat_eff_round=mat_eff_round)


def get_recipe(runs: float, type_id: int, price_mode='buy', average_mode=False, date=None, fuel_item_groups = None,
               mat_eff: float = 0, mat_eff_round: bool = False):
    recipe_model = read_database_recipe(type_id)
    if recipe_model is None:
        logging.warning(f'No recipe for type_id {type_id}.')
        return None
    return _get_recipe(runs, recipe_model, is_reaction(type_id), price_mode=price_mode, average_mode=average_mode,
                       date=date, fuel_item_groups=fuel_item_groups, mat_eff=mat_eff, mat_eff_round=mat_eff_round)


@timeit
def get_recipe_from_blueprint(runs: float, bp_id: int, price_mode='buy', average_mode=False, date=None, fuel_item_groups = None):
    recipe_model = read_database_recipe_from_bp(bp_id)
    if recipe_model is None:
        logging.warning(f'No recipe for bp_id {bp_id}.')
        return None
    return _get_recipe(runs, recipe_model, is_blueprint_reaction(bp_id), price_mode, average_mode=average_mode,
                       date=date, fuel_item_groups=fuel_item_groups)


@timeit
def _inputs_from_model(model: BlueprintBaseActivity, average_mode: bool = False):
    model_dict = model.to_primitive()
    inputs = model_dict['materials']

    if inputs is None:
        logging.warning(f'This recipe has no inputs. ({model_dict})')
        return None

    input_items = []
    for input in inputs:
        type_id = input['typeID']
        amount = input['quantity']
        item = get_item(type_id, amount=amount, min_amount=amount, average_mode=average_mode)
        if item.initialised is False:
            logging.warning('Failed to initialise input {type_id}. There are probably no orders. Cannot create recipe.')
            return None
        input_items.append(item)

    return input_items


@timeit
def _products_from_model(model: BlueprintBaseActivity, average_mode: bool = False):
    model_dict = model.to_primitive()
    prods = model_dict['products']

    if prods is None:
        logging.warning(f'This recipe has no products. ({model_dict})')
        return None

    if len(prods) > 1:
        raise AttributeError(f'This recipe has more than one product. ({model_dict})')
    type_id = prods[0]['typeID']
    amount = prods[0]['quantity']
    prod_item = get_item(type_id, amount=amount, min_amount=amount, average_mode=average_mode)
    if prod_item.initialised is False:
        logging.warning(f'Failed to initialise product {type_id}. There are probably no orders. Cannot create recipe.')
        return None
    return [prod_item]


