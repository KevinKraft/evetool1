#-------------------------------------

from src.object.item.eve_item import EveItem
from src.object.recipe.reprocessable_item import REPROCESSABLE_ITEMS
from src.object.item.mineral import MINERAL_ITEMS
from src.object.item.ore import ORE_ITEMS
from src.object.item.gas import GAS_ITEMS
from src.object.item.fuel import FUEL_ITEMS
from src.object.item.hybrid_reaction_products import HYBRID_REACTION_PRODUCT_ITEMS

#-------------------------------------

ALL_ITEMS = [EveItem] + REPROCESSABLE_ITEMS + MINERAL_ITEMS + ORE_ITEMS + GAS_ITEMS + FUEL_ITEMS + \
            HYBRID_REACTION_PRODUCT_ITEMS