#Script for plotting the recipe margin data over time from csv files.
#02/03/20

#-------------------------------------

import os
import logging
logging.getLogger().setLevel(logging.INFO)
import matplotlib.pyplot as plt
from matplotlib._color_data import TABLEAU_COLORS
import pandas
from math import ceil
import pickle
import datetime
import numpy as np
from scipy.signal import medfilt

#-------------------------------------

#RECIPE_TYPE = 'hybrid_polymer_average'
RECIPE_TYPE = 'hybrid_reactions'
#RECIPE_TYPE = 'composite'
#RECIPE_TYPE = 'hybrid_average'
#RECIPE_TYPE = 'composite_average'

DATA_FILE = 'data' + os.sep + RECIPE_TYPE + '.csv'
NAME_COL = 'reaction' #'bp' #'reaction'
MARGIN_COL = 'profit_margin'
PP_COL = 'profit_percentage'
PROFIT_COL = 'profit'
COST_PRICE_COL = 'cost_price'
TIMESTAMP_COL = 'timestamp'

BASE_FIG_PATH = 'data/plots/' + RECIPE_TYPE

DATA_TYPES = [MARGIN_COL, PP_COL, PROFIT_COL, COST_PRICE_COL]

PLOT_PROFITABLE = False

FROM_DATE = -1
TO_DATE = -1
#FROM_DATE = datetime.datetime(2019, 12, 1)
#TO_DATE = datetime.datetime(2020, 4, 5)

AVERAGE = True

SMOOTHING_DAYS = None #7

#-------------------------------------

NAME_ATTR = 'name'
STYLE_DICT = {
    MARGIN_COL: {NAME_ATTR: 'Margin (%)'},
    PP_COL: {NAME_ATTR: 'Profit Percentage (%)'},
    PROFIT_COL: {NAME_ATTR: 'Profit (ISK)'},
    COST_PRICE_COL: {NAME_ATTR: 'Cost Price (ISK)'},
}

CB_COLOUR_CYCLE = list(TABLEAU_COLORS.values())

#-------------------------------------


def smooth(y, kernel_size):
    #should be odd
    if kernel_size % 2 == 0:
        kernel_size += 1

    y = np.array(y)
    return medfilt(y, kernel_size).tolist()


def main():

    df = pandas.read_csv(DATA_FILE, delimiter=',')

    #filter timestamp
    df[TIMESTAMP_COL] = pandas.to_datetime(df[TIMESTAMP_COL])
    if AVERAGE is True:
        df[TIMESTAMP_COL] = df[TIMESTAMP_COL].dt.normalize()
    if FROM_DATE != -1:
        df = df.loc[df[TIMESTAMP_COL] >= FROM_DATE]
    if TO_DATE != -1:
        df = df.loc[df[TIMESTAMP_COL] <= TO_DATE]

    recipe_names = df[NAME_COL].unique()

    is_profitable_dict = {}
    for recipe in recipe_names:
        if recipe not in is_profitable_dict.keys():
            is_profitable_dict[recipe] = bool((df.loc[df[NAME_COL] == recipe][PROFIT_COL] >= 0).any())

    n_grid_y = len(DATA_TYPES)
    nheight = n_grid_y + 1

    fig, axs_ar = plt.subplots(nheight, 1)
    fig.set_figheight(15)
    fig.set_figwidth(15)

    axs = []
    for xi in range(0, nheight):
        axs.append(axs_ar[xi])

    handles, labels = None, None
    for plot_index in range(len(axs)):

        ax = axs[plot_index]

        if plot_index == len(axs)-1:
            ax.set_axis_off()
            if PLOT_PROFITABLE:
                legend_font_norm = len([1 for x in is_profitable_dict.values() if x is True])
            else:
                legend_font_norm = len(recipe_names)
            legend_font_size = 170 / legend_font_norm
            if legend_font_size > 18:
                legend_font_size = 18
            ax.legend(handles, labels, fontsize=legend_font_size)
            break

        if plot_index >= len(DATA_TYPES):
            continue
        else:
            data_type = DATA_TYPES[plot_index]

        name_dict = {}
        for name in df[NAME_COL]:

            if name in recipe_names:

                if PLOT_PROFITABLE is True:
                    if is_profitable_dict[name] is False:
                        continue

                if name not in name_dict.keys():
                    name_dict[name] = df.loc[df[NAME_COL] == name][[TIMESTAMP_COL, data_type]]

        data_tup_dict = {}
        for key, val in name_dict.items():
            data_tups = []
            for x, y in zip(list(val[TIMESTAMP_COL]), list(val[data_type])):
                data_tups.append((x,y))

                data_tups = sorted(data_tups, key=lambda x: x[0])

            data_tup_dict[key] = data_tups

        colour_cycle = CB_COLOUR_CYCLE
        n_lines = len(data_tup_dict.keys())
        if n_lines > len(colour_cycle):
            print("WARN: Not enough colours in the colour cycle.")
            cmap = plt.get_cmap('jet_r')
            colour_cycle = [ cmap(float(i)/n_lines) for i in range(0, n_lines)]

        for i, recipe_name in enumerate(data_tup_dict.keys()):

            xs, ys = zip(*data_tup_dict[recipe_name])
            #xs = [dateutil.parser.parse(x) for x in xs if isinstance(x,)]

            if SMOOTHING_DAYS:
                ys = smooth(ys, SMOOTHING_DAYS)

            ax.plot_date(xs, ys, '-', label=recipe_name, color=colour_cycle[i], linewidth=1.7)

            if 'ymin' in STYLE_DICT[data_type] and 'ymax' in STYLE_DICT[data_type]:
                ymin = STYLE_DICT[data_type]['ymin']
                ymax = STYLE_DICT[data_type]['ymax']
                ax.set_ylim([ymin, ymax])

            ax.grid(True)
            for tick in ax.get_xticklabels():
                tick.set_rotation(0)
            ax.tick_params(labelsize=8)

        ax.set_ylabel(STYLE_DICT[data_type][NAME_ATTR], fontsize=16)

        handles, labels = ax.get_legend_handles_labels()


    date_now = str(datetime.date.today()).replace('-','_')
    
    file_name = BASE_FIG_PATH
	
    date_sep = '_'
    if FROM_DATE != -1:
        date_from = str(FROM_DATE.date()).replace('-','_')
        file_name += '_from_' + date_from
        date_sep = '_on_'
    if TO_DATE != -1:
        date_to = str(TO_DATE.date()).replace('-','_')
        file_name += '_to_' + date_to
        date_sep = '_on_'
	
    file_name += date_sep + date_now

    fig.suptitle(file_name.split('/')[-1].replace('_', ' '), fontsize=24)
	
    pickle.dump(fig, open(file_name + '.pickle', 'wb'))

    plt.savefig(file_name + '.pdf')


if __name__ == '__main__':
    main()