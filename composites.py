#Tool for calculating margins for composites reactions

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.fuel_items import FUEL_GROUPS
from group_id_report import group_id_report

#-------------------------------------

GROUP_IDS = [429]

PRICE_MODE = 'sell'

RUNS = 1

SAVE_FILE = 'data/composites.csv'

SAVE_RESULTS = True

FUEL_ITEM_GROUPS = FUEL_GROUPS.COMPOSITES

#-------------------------------------

def main():

    group_id_report(GROUP_IDS, PRICE_MODE, RUNS, SAVE_FILE, SAVE_RESULTS, FUEL_ITEM_GROUPS)


if __name__ == '__main__':
    main()
