#Tool for calculating margins for making hybrid tech components at today's date
#08/05/20

#todo: The job cost in game is 10 times smaller, but it doesn't make much of a difference. The item base price was
# only 2 mil in game, but its more like 10 mil.

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.fuel_items import FUEL_GROUPS

from group_id_report import group_id_report

#-------------------------------------

GROUP_ID = 964

PRICE_MODE = 'sell'

RUNS = 1

SAVE_FILE = 'data/hybrid_tech_components.csv'

SAVE_RESULTS = True

FUEL_ITEM_GROUPS = FUEL_GROUPS.HYBRID_TECH_COMPONENT

MAT_EFF = 10

MAT_EFF_ROUND = False

#-------------------------------------

def main():

    group_id_report(GROUP_ID, PRICE_MODE, RUNS, SAVE_FILE, SAVE_RESULTS, fuel_item_groups=FUEL_ITEM_GROUPS,
                    mat_eff=MAT_EFF, mat_eff_round=MAT_EFF_ROUND)


if __name__ == '__main__':
    main()
