#are there any items in jita with higher buy prices than sell prices?

#POTENTIAL OBSOLETE, AND NOT SURE IF IT STILL WORKS

#-------------------------------------

from src.api.api import get_items_for_order, buy_sell_profit

#-------------------------------------


def main():

    items_for_order = get_items_for_order()
    profits = []

    print(len(items_for_order))

    for item_id in items_for_order[100:1000]:
        profit = buy_sell_profit(item_id)
        if profit != None:
            profits.append((item_id, profit))
            if profit > 0 : print('Profit of {} is {}.'.format(item_id, profit.mil))

    profits = sorted(profits, key=lambda x: x[1], reverse=True)

    for profit in profits:
        print('{}: {}'.format(profit[0], profit[1].mil))


if __name__ == '__main__':
    main()