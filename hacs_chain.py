#Tool for calculating margins for composites reactions

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.id import GROUP_ID
from group_id_chain_report import group_id_chain_report

#-------------------------------------

GROUP_IDS = GROUP_ID.HEAVY_ASSAULT_CRUISER

RUNS = 1

SAVE_FILE = 'data/hacs_chain.csv'

SAVE_RESULTS = False

#-------------------------------------

def main():

    group_id_chain_report(GROUP_IDS, RUNS, SAVE_FILE, SAVE_RESULTS)


if __name__ == '__main__':
    main()
