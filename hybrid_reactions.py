#Tool for calculating margins hybrid polymers at today's date
#08/05/20

#todo: The job cost in game is 10 times smaller, but it doesn't make much of a difference. The item base price was
# only 2 mil in game, but its more like 10 mil.

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.id import ITEM_ID
from src.constants.fuel_items import FUEL_GROUPS
from src.metadata.read import get_group as read_database_group

from group_id_report import group_id_report

#-------------------------------------

GROUP_ID = 974

PRICE_MODE = 'buy'

RUNS = 1

SAVE_FILE = 'data/hybrid_reactions.csv'

SAVE_RESULTS = True

FUEL_ITEM_GROUP = FUEL_GROUPS.HYBRID_POLYMER

#-------------------------------------

def main():

    group_id_report(GROUP_ID, PRICE_MODE, RUNS, SAVE_FILE, SAVE_RESULTS, FUEL_ITEM_GROUP)


if __name__ == '__main__':
    main()
