#Tool for calculating margins for all types of a given group at today's date
#08/05/20

#todo: The job cost in game is 10 times smaller, but it doesn't make much of a difference. The item base price was
# only 2 mil in game, but its more like 10 mil.

#-------------------------------------

import logging
import datetime

from src.constants.id import STRUCTURE_ID
from src.constants.tax_rate import TAX_RATE

from src.write.save_as_df import SaveRowAsCsv
from src.metadata.read import get_all_types as read_database_all_types
from src.object.recipe.recipe import get_recipe

#-------------------------------------

STRUCTURE = STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER
#STRUCTURE = STRUCTURE_ID.JITA_TRADE_HUB

SALEX_TAX = TAX_RATE[STRUCTURE]['sales']
BROKER_TAX = TAX_RATE[STRUCTURE]['broker']
FULL_SALES_TAX = SALEX_TAX + BROKER_TAX

SYSTEM_COST_INDEX = 0.03 #3%
FACILITY_TAX_RATE = 0.1 #10%

#-------------------------------------

def group_id_report(group_id_or_list, price_mode, runs, save_file, save_results, fuel_item_groups=None, mat_eff=0,
                    mat_eff_round=False):

    if isinstance(group_id_or_list, int):
        group_ids = [group_id_or_list]
    else:
        group_ids = group_id_or_list

    product_ids = []
    for gid in group_ids:
        product_ids += read_database_all_types(gid)

    profits = []
    profit_margins = []
    profit_percentages = []
    markups = []
    cost_prices = []
    reaction_names = []

    for product_id in product_ids:
        recipe = get_recipe(runs, product_id, price_mode=price_mode, fuel_item_groups=fuel_item_groups,
                            mat_eff=mat_eff, mat_eff_round=mat_eff_round)

        if recipe is None:
            logging.warning(f'Did not find recipe for product_id {product_id}.')
            continue

        recipe.job_summary(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)
        profits.append((recipe.name, recipe.profit(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)))
        profit_margins.append((recipe.name, recipe.profit_margin(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)))
        profit_percentages.append((recipe.name, recipe.profit_percentage(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                         FACILITY_TAX_RATE)))
        markups.append((recipe.name, recipe.markup(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)))
        cost_prices.append((recipe.name, recipe.cost_price(FULL_SALES_TAX)))
        reaction_names.append(recipe.name)


        print('*'*80)

    if save_results:

        cols = ['timestamp', 'reaction', 'profit', 'profit_margin', 'profit_percentage', 'markup', 'cost_price', 'runs',
                'system_cost_index', 'facility_tax_rate', 'full_sales_tax', 'buy_inputs']

        if mat_eff > 0:
            cols += ['mat_eff', 'mat_eff_round']

        csv_saver = SaveRowAsCsv(save_file, cols)

        def _fv(val):
            return round(val[1], 2)

        def _assert_match(val, name):
            assert val[0] == name

        for p, pm, pp, mu, cp, name in zip(profits, profit_margins, profit_percentages, markups, cost_prices,
                                           reaction_names):
            _assert_match(p, name)
            _assert_match(pm, name)
            _assert_match(pp, name)
            _assert_match(mu, name)
            _assert_match(cp, name)

            col_vals = [datetime.datetime.now().isoformat(), name, _fv(p), _fv(pm), _fv(pp), _fv(mu), _fv(cp), runs,
                        SYSTEM_COST_INDEX, FACILITY_TAX_RATE, FULL_SALES_TAX, price_mode]

            if mat_eff > 0:
                col_vals += [mat_eff, mat_eff_round]

            csv_saver.add_data(col_vals)


        csv_saver.write()

    print('Profit:')
    profits = sorted(profits, key=lambda x: x[1], reverse=True)
    for name, profit in profits:
        print(name, profit)
    print('Profit margins:')
    vals = sorted(profit_margins, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val, 2)))
    print('Profit percentages:')
    vals = sorted(profit_percentages, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val,2)))
    print('Markup:')
    vals = sorted(markups, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val,2)))
    print('Cost prices:')
    vals = sorted(cost_prices, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val,2)))
