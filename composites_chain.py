#Tool for calculating margins for composites reactions

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.id import GROUP_ID
from group_id_chain_report import group_id_chain_report

#-------------------------------------

GROUP_IDS = GROUP_ID.COMPOSITE

RUNS = 1

SAVE_FILE = 'data/composites_chain.csv'

SAVE_RESULTS = True

#-------------------------------------

def main():

    group_id_chain_report(GROUP_IDS, RUNS, SAVE_FILE, SAVE_RESULTS)


if __name__ == '__main__':
    main()
