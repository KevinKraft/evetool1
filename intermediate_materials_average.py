#Tool for saving historic composite reaction data to a csv file
#09/03/20

#OBSOLETE AND WRONG

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)
import datetime

from src.constants.id import STRUCTURE_ID
from src.constants.tax_rate import TAX_RATE

from src.object.recipe.composite_reaction import *
from src.write.save_as_df import SaveRowAsCsv

#-------------------------------------

#STRUCTURE = STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER
STRUCTURE = STRUCTURE_ID.JITA_TRADE_HUB

SALEX_TAX = TAX_RATE[STRUCTURE]['sales']
BROKER_TAX = TAX_RATE[STRUCTURE]['broker']
FULL_SALES_TAX = SALEX_TAX + BROKER_TAX

SYSTEM_COST_INDEX = 0.0511#0.0295 #3.09%
FACILITY_TAX_RATE = 0.032 #0.1 #10%

REACTIONS = [
    CaesariumCadmideReactionFormula,
    CarbonPolymersReactionFormula,
    CeramicPowderReactionFormula,
    CrystalliteAlloyReactionFormula,
    DysporiteReactionFormula,
    FerniteAlloyReactionFormula,
    FerrofluidReactionFormula,
    FluxedCondensatesReactionFormula,
    HexiteReactionFormula,
    HyperfluriteReactionFormula,
    NeoMercuriteReactionFormula,
    PlatinumTechniteReactionFormula,
    PromethiumMercuriteReactionFormula,
    PrometiumReactionFormula,
    RolledTungstenAlloyReactionFormula,
    SiliconDiboriteReactionFormula,
    SoleriumReactionFormula,
    SulfuricAcidReactionFormula,
    ThuliumHafniteReactionFormula,
    TitaniumChromideReactionFormula,
]

RUNS = 1

PRICE_MODE = 'sell'

SAVE_RESULTS = True

#-------------------------------------

def main():

    csv_saver = SaveRowAsCsv('data/composite_average_reactions.csv', ['timestamp', 'reaction', 'profit',
                                                                      'profit_margin', 'profit_percentage',
                                                                      'markup', 'cost_price', 'runs',
                                                                      'system_cost_index', 'facility_tax_rate',
                                                                      'full_sales_tax', 'buy_inputs'])

    start_date = datetime.date(year=2020, month=4, day=4)
    #inclusive_end_date = datetime.date(year=2019, month=2, day=1) #this is the oldest date the api has data
    inclusive_end_date = datetime.date(year=2020, month=3, day=8)

    this_date = start_date
    while this_date >= inclusive_end_date:

        print('Running for date {0}'.format(this_date))

        profits = []
        profit_margins = []
        profit_percentages = []
        markups = []
        cost_prices = []
        reaction_names = []

        for reaction_constr in REACTIONS:
            reaction = reaction_constr(RUNS, PRICE_MODE, average_mode=True, date=this_date)

            if reaction.has_data_on_date() is False:
                logging.warning('One or more items for {0} has no average price data for date {1}'
                      .format(reaction_constr.__name__, this_date))
                continue

            profits.append((reaction.name, reaction.profit(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)))
            profit_margins.append((reaction.name, reaction.profit_margin(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                         FACILITY_TAX_RATE)))
            profit_percentages.append((reaction.name, reaction.profit_percentage(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                         FACILITY_TAX_RATE)))
            markups.append((reaction.name, reaction.markup(FULL_SALES_TAX, SYSTEM_COST_INDEX,
                                                                         FACILITY_TAX_RATE)))
            cost_prices.append((reaction.name, reaction.cost_price(FULL_SALES_TAX)))
            reaction_names.append(reaction.__class__.__name__)

        if SAVE_RESULTS:

            def _fv(val):
                return round(val[1], 2)

            for p, pm, pp, mu, cp, name in zip(profits, profit_margins, profit_percentages, markups, cost_prices,
                                               reaction_names):
                csv_saver.add_data([this_date, name,
                                    _fv(p), _fv(pm), _fv(pp), _fv(mu), _fv(cp),
                                    RUNS, SYSTEM_COST_INDEX, FACILITY_TAX_RATE, FULL_SALES_TAX,
                                    PRICE_MODE])

        this_date = this_date + timedelta(days=-1)


if __name__ == '__main__':
    main()