#Tool for calculating margins for all types of a given group at today's date and following all chains in the
#chain config
#01/08/20

#-------------------------------------

import datetime

from src.constants.id import STRUCTURE_ID
from src.constants.tax_rate import TAX_RATE

from src.write.save_as_df import SaveRowAsCsv
from src.metadata.read import get_all_types as read_database_all_types
from src.object.recipe_chain.recipe_chain import RecipeChain, RecipeChainInitException

#-------------------------------------

STRUCTURE = STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER
#STRUCTURE = STRUCTURE_ID.JITA_TRADE_HUB

SALEX_TAX = TAX_RATE[STRUCTURE]['sales']
BROKER_TAX = TAX_RATE[STRUCTURE]['broker']
FULL_SALES_TAX = SALEX_TAX + BROKER_TAX

SYSTEM_COST_INDEX = 0.03 #3%
FACILITY_TAX_RATE = 0.1 #10%

#-------------------------------------

def group_id_chain_report(group_id_or_list, runs, save_file, save_results):

    if isinstance(group_id_or_list, int):
        group_ids = [group_id_or_list]
    else:
        group_ids = group_id_or_list

    product_ids = []
    for gid in group_ids:
        product_ids += read_database_all_types(gid)

    profits = []
    profit_margins = []
    profit_percentages = []
    markups = []
    cost_prices = []
    names = []

    for product_id in product_ids:

        try:
            chain = RecipeChain(product_id, runs=runs)
        except RecipeChainInitException:
            continue

        profits.append((chain.name, chain.profit(FULL_SALES_TAX, FACILITY_TAX_RATE)))
        profit_margins.append((chain.name, chain.profit_margin(FULL_SALES_TAX, FACILITY_TAX_RATE)))
        profit_percentages.append((chain.name, chain.profit_percentage(FULL_SALES_TAX, FACILITY_TAX_RATE)))
        markups.append((chain.name, chain.markup(FULL_SALES_TAX, FACILITY_TAX_RATE)))
        cost_prices.append((chain.name, chain.cost_price(FULL_SALES_TAX, FACILITY_TAX_RATE)))
        names.append(chain.name)

        print('*'*80)

    if save_results:

        #save a pickle of the config
        pickle_file = f'data/config_pickles/config_backup_{datetime.datetime.now().strftime("%Y%m%d-%H%M%S")}.pickle'
        chain.pickle_config(pickle_file)

        cols = ['timestamp', 'chain', 'profit', 'profit_margin', 'profit_percentage', 'markup', 'cost_price', 'runs',
                'facility_tax_rate', 'full_sales_tax']


        csv_saver = SaveRowAsCsv(save_file, cols)

        def _fv(val):
            return round(val[1], 2)

        def _assert_match(val, name):
            assert val[0] == name

        for p, pm, pp, mu, cp, name in zip(profits, profit_margins, profit_percentages, markups, cost_prices, names):
            _assert_match(p, name)
            _assert_match(pm, name)
            _assert_match(pp, name)
            _assert_match(mu, name)
            _assert_match(cp, name)

            col_vals = [datetime.datetime.now().isoformat(), name, _fv(p), _fv(pm), _fv(pp), _fv(mu), _fv(cp), runs,
                        FACILITY_TAX_RATE, FULL_SALES_TAX]

            csv_saver.add_data(col_vals)

        csv_saver.write()

    print('Profit:')
    profits = sorted(profits, key=lambda x: x[1], reverse=True)
    for name, profit in profits:
        print(name, profit)
    print('Profit margins:')
    vals = sorted(profit_margins, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val, 2)))
    print('Profit percentages:')
    vals = sorted(profit_percentages, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val,2)))
    print('Markup:')
    vals = sorted(markups, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val,2)))
    print('Cost prices:')
    vals = sorted(cost_prices, key=lambda x: x[1], reverse=True)
    for name, val in vals:
        print(name, str(round(val,2)))
