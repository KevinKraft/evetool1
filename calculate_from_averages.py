#Script for reading historic average price data from csv files and calculating the mean and std.
#16/05/20

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)
import pandas
from statistics import mean, stdev

from src.util.diagnostic import Timer

from src.write.save_as_df import CSVIOWriter

#-------------------------------------

DATA_FILE = 'data/all_recipes_average.csv'
NAME_COL = 'bp'
MARGIN_COL = 'profit_margin'
PP_COL = 'profit_percentage'
PROFIT_COL = 'profit'
COST_PRICE_COL = 'cost_price'
TIMESTAMP_COL = 'timestamp'
BP_ID_COL = 'bp_id'
PRODUCT_ID_COL = 'product_id'
GROUP_ID_COL = 'group_id'
GROUP_NAME_COL = 'group_name'

#It is assumed that these values are all the same in the csv data. If not this will be wrong.
FIXED_SYSTEM_COST_INDEX = 0.03
FIXED_FACILITY_TAX_RATE = 0.1
FIXED_FULL_SALES_TAX = 0.05
FIXED_BUY_INPUTS = 'buy'
FIXED_RUNS = 1

SAVE_PATH = 'data/all_recipes_average_results.csv'

SAVE_TO_FILE = True

NEW_FILE = True

Timer.running = False

#-------------------------------------

def main():

    df_full = pandas.read_csv(DATA_FILE, delimiter=',', encoding='windows-1252')

    bp_names = df_full[NAME_COL].unique()

    csv_saver = None
    if SAVE_TO_FILE:

        csv_saver = CSVIOWriter(SAVE_PATH, ['bp',
                                            'mean_profit', 'std_profit',
                                            'mean_profit_margin', 'std_profit_margin',
                                            'mean_profit_percentage', 'std_profit_percentage',
                                            'mean_cost_price', 'std_cost_price',
                                            'runs', 'system_cost_index', 'facility_tax_rate', 'full_sales_tax',
                                            'buy_inputs', 'bp_id', 'product_id', 'group_id', 'group_name'],
                                new_file=NEW_FILE)

    for i, bp_name in enumerate(bp_names):

        profits = []
        profit_margins = []
        profit_percentages = []
        cost_prices = []

        df = df_full.loc[df_full[NAME_COL] == bp_name]

        first_row = df.iloc[0]
        bp_id = first_row[BP_ID_COL]
        product_id = first_row[PRODUCT_ID_COL]
        group_id = first_row[GROUP_ID_COL]
        group_name = first_row[GROUP_NAME_COL]

        for _, row in df.iterrows():

            profits.append(row[PROFIT_COL])
            profit_margins.append(row[MARGIN_COL])
            profit_percentages.append(row[PP_COL])
            cost_prices.append(row[COST_PRICE_COL])


        x_p = mean(profits)
        if len(profits) < 2:
            s_p = None
        else:
            s_p = stdev(profits, x_p)

        x_pm = mean(profit_margins)
        if len(profit_margins) < 2:
            s_pm = None
        else:
            s_pm = stdev(profit_margins, x_pm)

        x_pp = mean(profit_percentages)
        if len(profit_percentages) < 2:
            s_pp = None
        else:
            s_pp = stdev(profit_percentages, x_pp)

        x_cp = mean(cost_prices)
        if len(cost_prices) < 2:
            s_cp = None
        else:
            s_cp = stdev(cost_prices, x_cp)


        if SAVE_TO_FILE:

            def _fv(val):
                if val is None:
                    return 'None'
                return round(val, 2)


            csv_saver.add_data([bp_name,
                                _fv(x_p), _fv(s_p), _fv(x_pm), _fv(s_pm), _fv(x_pp), _fv(s_pp), _fv(x_cp), _fv(s_cp),
                                FIXED_RUNS, FIXED_SYSTEM_COST_INDEX, FIXED_FACILITY_TAX_RATE, FIXED_FULL_SALES_TAX,
                                FIXED_BUY_INPUTS, bp_id, product_id, group_id, group_name])

    csv_saver.write()
    csv_saver.finish()

    Timer.results()


if __name__ == '__main__':
    main()