#script for generating a monthly report
#09/03/20

#Todo:
# add uncertainties to values - done
# line plot of ROI per day
# line plot of profit per day
# use start and end dates

#-------------------------------------

import os
import logging
logging.getLogger().setLevel(logging.INFO)
import pandas
import datetime
import numpy as np
from statistics import stdev

from src.constants.id import ITEM_ID
from src.object.item.eve_item import get_item
from src.util.isk_amount import ISKAmount

#-------------------------------------

FROM_DATE = -1
TO_DATE = datetime.datetime(2020, 3, 31)

RESULTS_LOC = 'data/report/monthly_2020-05'
#RESULTS_LOC = 'data/report/total_2020-05-31'

#-------------------------------------

DATA_FILE = 'data'+os.sep+'manual_profit.csv'

BUY_DATE_COL = 'buy_date'
REACTION_COL = 'reaction'
BUY_LOC_COL = 'buy_location'
PROFIT_COL  = 'profit'
SELL_LOC_COL = 'location'
TOTAL_INPUT_COL = 'total_input'
TOTAL_OUTPUT_COL = 'total_output'

TYPE_MAP = {
    BUY_DATE_COL : str,
    REACTION_COL : str,
    BUY_LOC_COL : str,
    PROFIT_COL : np.float64,
    SELL_LOC_COL : str,
    TOTAL_INPUT_COL : np.float64,
    TOTAL_OUTPUT_COL : np.float64
}

PARSE_DATES = [BUY_DATE_COL]

ROI_COL = 'roi'

REACTION_MAP = {
    'sm': get_item(ITEM_ID.SCANDIUM_METALLOFULLERENE),
    'mf': get_item(ITEM_ID.METHANOFULLERENE),
    'fig': get_item(ITEM_ID.FULLERENE_INTERCALATED_GRAPHITE),
    'ppd': get_item(ITEM_ID.PPD_FULLERENE_FIBERS),
    'lm': get_item(ITEM_ID.LANTHANUM_METALLOFULLERENE),
    'ff': get_item(ITEM_ID.FULLEROFERROCENE),
}

RESULTS = {}

TOTAL_ROI_KEY = 'total_roi'
PROFIT_BY_REACTION_KEY = 'profit_by_reaction'
TOTAL_PROFIT_KEY = 'total_profit'
ROI_BY_REACTION_KEY = 'roi_by_reaction'
ROI_BY_REACTION_STD_KEY = 'roi_by_reaction_std'
ROI_BY_BUY_SELL_LOC_KEY = 'roi_by_buy_sell_loc'
ROI_BY_BUY_SELL_LOC_STD_KEY = 'roi_by_buy_sell_loc_std'

STYLE_DICT = {
    TOTAL_PROFIT_KEY: 'isk_amount',
    PROFIT_BY_REACTION_KEY: 'dict_isk_amount',
    TOTAL_ROI_KEY: 'percentage',
    ROI_BY_REACTION_KEY: 'dict_percentage',
    ROI_BY_REACTION_STD_KEY: 'dict_percentage',
    ROI_BY_BUY_SELL_LOC_KEY: 'dict_percentage',
    ROI_BY_BUY_SELL_LOC_STD_KEY: 'dict_percentage'
}


#-------------------------------------

def totalProfit(df):
    return float(df[PROFIT_COL].sum())


def totlaROI(df):
    return float((totalProfit(df) / df[TOTAL_INPUT_COL].sum() + 0.0) * 100)


def calcROI(row):
    if isinstance(row, pandas.DataFrame):
        if (row[TOTAL_INPUT_COL] == 0).all():
            return None
    if isinstance(row, pandas.Series):
        if row[TOTAL_INPUT_COL] == 0:
            return None
    return (row[PROFIT_COL] / row[TOTAL_INPUT_COL] + 0.0) * 100.0


# def styleResults(k, val=None, tabs=2, style_dict=STYLE_DICT):
#     tab = ' '*tabs
#     if isinstance(k, dict):
#         for key, value in k.items():
#             styleResults(key, value, tabs=tabs+2, style_dict=style_dict[k])
#     else:
#         if STYLE_DICT[k] == 'percentage':
#             value = str(round(val)) + '%'
#         elif STYLE_DICT[k] == 'isk_amount':
#             value = str(ISKAmount(val))
#         else:
#             raise ValueError(f'{k} is not a recognised output style')
#         print(tab + k + ': ' + value)


def groupKey(df, key_cols):
    if not isinstance(key_cols, list):
        key_cols = [key_cols]
    return df.groupby(key_cols)


def groupKeyBySum(df, key_cols):
    df = groupKey(df, key_cols)
    return df.sum()


def groupReactionByROI(df):
    sdf = groupKeyBySum(df, REACTION_COL)
    ret_dict = {}
    for reac in REACTION_MAP.keys():
        if not reac in sdf.index:
            continue
        row = sdf.loc[reac]
        roi = calcROI(row)
        ret_dict[REACTION_MAP[reac].name] = roi
    return ret_dict


def groupReactionByROIStd(df):
    ret_dict = {}
    for reac in REACTION_MAP.keys():
        rdf = df.loc[df[REACTION_COL] == reac]
        rois = calcROI(rdf)
        if rois is None:
            res = None
        else:
            rois = [roi for roi in rois.tolist() if not roi == np.inf]
            if len(rois) < 2:
                res = None
            else:
                res = stdev(rois)
        ret_dict[REACTION_MAP[reac].name] = res
    return ret_dict


def groupBuySellLocByROIStd(df):
    buy_locs = df[BUY_LOC_COL].unique()
    sell_locs = df[SELL_LOC_COL].unique()
    pair_map = {}
    for buy_loc in buy_locs:
        for sell_loc in sell_locs:
            tdf = df.loc[(df[BUY_LOC_COL] == buy_loc) & (df[SELL_LOC_COL] == sell_loc)]
            if len(tdf.index) == 0:
                continue
            rois = []
            for _, row in tdf.iterrows():
                rois.append(calcROI(row))
            if len(rois) > 1:
                res = stdev(rois)
            else:
                res = None
            pair_map[buy_loc + '/' + sell_loc] = res
    return pair_map


def groupBuySellLocByROI(df):
    sdf = groupKeyBySum(df, [BUY_LOC_COL, SELL_LOC_COL])
    ret_dict = {}
    for index_tup, row in sdf.iterrows():
        name = index_tup[0] + '/' + index_tup[1]
        roi = calcROI(row)
        ret_dict[name] = roi
    return ret_dict


def groupReactionByProfit(df):
    sdf = groupKeyBySum(df, REACTION_COL)
    ret_dict = {}
    for name, row in sdf.iterrows():
        profit = row[PROFIT_COL]
        ret_dict[REACTION_MAP[name].name] = profit
    return ret_dict


def print_and_store(strval, store):
    store += strval + '\n'
    print(strval)
    return store


def main():

    df = pandas.read_csv(DATA_FILE, delimiter=',', dtype=TYPE_MAP, parse_dates=PARSE_DATES)

    df[ROI_COL] = df.apply(lambda row: calcROI(row), axis=1)

    RESULTS[TOTAL_PROFIT_KEY] = totalProfit(df)
    RESULTS[PROFIT_BY_REACTION_KEY] = groupReactionByProfit(df)
    RESULTS[TOTAL_ROI_KEY] = totlaROI(df)
    RESULTS[ROI_BY_REACTION_KEY] = groupReactionByROI(df)
    RESULTS[ROI_BY_REACTION_STD_KEY] = groupReactionByROIStd(df)
    RESULTS[ROI_BY_BUY_SELL_LOC_KEY] = groupBuySellLocByROI(df)
    RESULTS[ROI_BY_BUY_SELL_LOC_STD_KEY] = groupBuySellLocByROIStd(df)

    tab = ' '*2
    text_results = ''
    text_results = print_and_store('Results:', text_results)
    for key, value in RESULTS.items():
        if STYLE_DICT[key] == 'percentage':
            value = str(round(value)) + '%'
        elif STYLE_DICT[key] == 'isk_amount':
            value = str(ISKAmount(value))
        elif STYLE_DICT[key] == 'dict_percentage':
            sv = '\n'
            for k, v in value.items():
                if v is None:
                    sv += tab * 2 + k + ': None \n'
                else:
                    sv += tab * 2 + k + ': ' + str(round(v)) + '% \n'
            value = sv
        elif STYLE_DICT[key] == 'dict_isk_amount':
            sv = '\n'
            for k, v in value.items():
                sv += tab*2 + k + ': ' + str(ISKAmount(v)) + '\n'
            value = sv
        else:
            raise ValueError(f'{key} is not a recognised output style')
        text_results = print_and_store(tab + key + ': ' + value, text_results)


    text_results_path = RESULTS_LOC + '.txt'
    if os.path.isfile(text_results_path):
        os.remove(text_results_path)

    with open(text_results_path, 'w') as f:
        f.write(text_results)





if __name__ == '__main__':
    main()