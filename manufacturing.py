#Tool for calculating various manufacturing margins
#06/12/20

#todo: The job cost in game is 10 times smaller, but it doesn't make much of a difference. The item base price was
# only 2 mil in game, but its more like 10 mil.

#OBSOLETE

#-------------------------------------

import logging
logging.getLogger().setLevel(logging.INFO)

from src.constants.id import STRUCTURE_ID
from src.constants.tax_rate import TAX_RATE

from src.object.recipe.blueprints import *

#-------------------------------------

STRUCTURE = STRUCTURE_ID.PERIMETER_TRANQUILITY_TRADING_TOWER
#STRUCTURE = STRUCTURE_ID.JITA_TRADE_HUB

SALEX_TAX = TAX_RATE[STRUCTURE]['sales']
BROKER_TAX = TAX_RATE[STRUCTURE]['broker']
FULL_SALES_TAX = SALEX_TAX + BROKER_TAX

SYSTEM_COST_INDEX = 0.0386 #3.86% Korama
FACILITY_TAX_RATE = 0.0 #0%

BLUEPRINTS = [N125MMRailgunIBlueprint, N125MMRailgunIIBlueprint]

#-------------------------------------

def main():

    profits = []

    for reaction_constr in BLUEPRINTS:
        reaction = reaction_constr(8)
        reaction.job_summary(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)
        profits.append((reaction.name, reaction.profit(FULL_SALES_TAX, SYSTEM_COST_INDEX, FACILITY_TAX_RATE)))

    profits = sorted(profits, key=lambda x: x[1], reverse=True)

    for name, profit in profits:
        print(name, profit)




if __name__ == '__main__':
    main()