#Script for plotting the recipe pull data over time from csv files.
#09/03/20

#OBSOLETE

#-------------------------------------

import os
import logging
logging.getLogger().setLevel(logging.INFO)
import matplotlib.pyplot as plt
from matplotlib._color_data import TABLEAU_COLORS
import pandas
from math import ceil
import pickle
import datetime

from src.object.recipe.composite_reaction import *

#-------------------------------------

#RECIPE_TYPE = 'hybrid' #don't make sense as not evenly spaced
#RECIPE_TYPE = 'composite' #don't make sense as not evenly spaced
#RECIPE_TYPE = 'hybrid_average'
#RECIPE_TYPE = 'composite_average'
RECIPE_TYPE = 'composite_average_buy'

DATA_FILE = 'data'+os.sep+RECIPE_TYPE+'_reactions.csv'
NAME_COL = 'reaction'
MARGIN_COL = 'profit_margin'
PP_COL = 'profit_percentage'
PROFIT_COL = 'profit'
MARKUP_COL = 'markup'
COST_PRICE_COL = 'cost_price'
TIMESTAMP_COL = 'timestamp'

FIG_PATH = 'data/plots/' + RECIPE_TYPE + '_pulls'

FIG_NAME = RECIPE_TYPE + ' distributions'

#RECIPE_NAME = FullereneIntercalatedGraphiteReaction.__name__
#RECIPE_NAME = ScandiumMetallofullereneReaction.__name__
RECIPE_NAME = CarbonPolymersReactionFormula.__name__

DATA_TYPES = [MARGIN_COL, PP_COL, PROFIT_COL, MARKUP_COL, COST_PRICE_COL]

PLOT_PROFITABLE = False

FROM_DATE = datetime.date(year=2019, month=2, day=1)
#FROM_DATE = datetime.date(year=2020, month=3, day=1)
#FROM_DATE = datetime.date(year=2020, month=2, day=7)
TO_DATE = datetime.date(year=2020, month=3, day=7)

#37 bins is about 10 days per bin over a year long interval.
NBINS = 37
#NBINS = 12
#NBINS = 5

#-------------------------------------

NAME_ATTR = 'name'
STYLE_DICT = {
    MARGIN_COL : {NAME_ATTR: 'Margin (%)'},
    PP_COL: {NAME_ATTR: 'Profit Percentage (%)'},
    PROFIT_COL: {NAME_ATTR: 'Profit (ISK)'},
    MARKUP_COL: {NAME_ATTR: 'Markup (%)'},
    COST_PRICE_COL: {NAME_ATTR: 'Cost Price (ISK)'},
}

CB_COLOUR_CYCLE = list(TABLEAU_COLORS.values())



#-------------------------------------

def main():

    df = pandas.read_csv(DATA_FILE, delimiter=',')

    is_profitable_dict = {}
    for recipe in [RECIPE_NAME]:
        if recipe not in is_profitable_dict.keys():
            is_profitable_dict[recipe] = bool((df.loc[df[NAME_COL] == recipe][PROFIT_COL] >= 0).any())

    nheight = int(ceil(len(DATA_TYPES)/2))

    fig, axs_ar = plt.subplots(nheight, 2)
    fig.set_figheight(15)
    fig.set_figwidth(15)

    axs = []
    for xi in range(0, nheight):
        for yi in range(0, 2):
            axs.append(axs_ar[xi, yi])

    handles, labels = None, None
    for plot_index in range(len(axs)):

        ax= axs[plot_index]

        if plot_index == len(axs)-1:
            ax.set_axis_off()
            if PLOT_PROFITABLE:
                legend_font_norm = len([1 for x in is_profitable_dict.values() if x is True])
            else:
                legend_font_norm = len([RECIPE_NAME])
            legend_font_size = 170 / legend_font_norm
            if legend_font_size > 18:
                legend_font_size = 18
            ax.legend(handles, labels, fontsize=legend_font_size)
            break

        data_type = DATA_TYPES[plot_index]

        name_dict = {}
        for name in df[NAME_COL]:

            if name in [RECIPE_NAME]:

                if PLOT_PROFITABLE is True:
                    if is_profitable_dict[name] is False:
                        continue

                to_date = str(TO_DATE)
                from_date = str(FROM_DATE)
                if name not in name_dict.keys():
                    ndf = df.loc[(df[NAME_COL] == name) &
                                 (df[TIMESTAMP_COL] >= from_date) &
                                 (df[TIMESTAMP_COL] <= to_date)]
                    name_dict[name] = ndf[data_type]

        data_dict = {}
        for key, val in name_dict.items():
            xs = sorted(list(val))
            data_dict[key] = xs

        colour_cycle = CB_COLOUR_CYCLE
        n_lines = len(data_dict.keys())
        if n_lines > len(colour_cycle):
            print("WARN: Not enough colours in the colour cycle.")
            cmap = plt.get_cmap('jet_r')
            colour_cycle = [ cmap(float(i)/n_lines) for i in range(0, n_lines)]

        for i, recipe_name in enumerate(data_dict.keys()):

            xs = data_dict[recipe_name]
            ax.hist(xs, bins=NBINS, label=recipe_name, color=colour_cycle[i], alpha=0.5)

            ax.grid(True)

        ax.set_xlabel(STYLE_DICT[data_type][NAME_ATTR], fontsize=16)

        handles, labels = ax.get_legend_handles_labels()


    date_now = str(datetime.date.today()).replace('-','_')

    fig_name = FIG_NAME + ' from ' + str(FROM_DATE) + ' to ' + str(TO_DATE)
    fig.suptitle(fig_name, fontsize=24)

    file_name = FIG_PATH + '_' + RECIPE_NAME + '_' + date_now

    pickle.dump(fig, open(file_name + '.pickle', 'wb'))

    plt.savefig(file_name + '.pdf')


if __name__ == '__main__':
    main()